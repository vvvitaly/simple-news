/**
 * Created by psi on 24.01.17.
 */

;(function($) {
    "use strict";

    /**
     * Hide element if value of target not in displayValues
     * @param target
     * @param displayValues
     * @returns {jQuery}
     */
    $.fn.hideOn = function(target, displayValues) {
        return this.each(function () {
            var $target = $(target),
                $this = $(this),
                // TODO: hardcoded for bootstrap3
                $control = $this.parent();

            if (displayValues.indexOf($target.val()) < 0) {
                $control.hide();
            }
            $target.on('change', function() {
                displayValues.indexOf($target.val()) >= 0? $control.show(): $control.hide();
            });
        });
    };


    /**
     * ReBuild target select when value changed
     * @param target
     * @param values
     */
    $.fn.buildSelect = function(target, values) {
        function getOptions(values) {
            var options = [];
            for (var v in values) {
                options.push('<option value="' + v + '">' + values[v] + '</option>');
            }
            return options.join();
        }

        return this.each(function () {
            var $target = $(target),
                $this = $(this);

            if ($this.val() && $this.val() in values) {
                $target.html(getOptions(values[$this.val()]));
            }

            $this.on('change', function () {
                var val = $this.val(),
                    newValues = val in values? values[val]: {};
                $target.html(getOptions(newValues));
            });
        });
    };

    /**
     * @param options
     * @returns {*}
     */
    $.fn.notificationForm = function(options) {
        var settings = $.extend({
            recipientSelectId: "",
            recipientsWithRoles: [],
            roleSelectId: "",
            targetSelectId: "",
            eventSelectId: "",
            events: {},
            transportSelectId: "",
            transportMessageFormUrl: "",
            transportFormContainer: ""
        }, options);

        return this.each(function() {
            var $this = $(this);

            $this.find('#' + settings.roleSelectId)
                .hideOn('#' + settings.recipientSelectId, settings.recipientsWithRoles);
            $this.find('#' + settings.targetSelectId).buildSelect('#' + settings.eventSelectId, settings.events);
        });
    };

    $.fn.sendForm = function(options) {
        var settings = $.extend({
            recipientTypeInput: ""
        }, options);

        return this.each(function() {
            var $this = $(this),
                $recipientType = $this.find(settings.recipientTypeInput);

            $this.find('[data-recipient-type]').on('click', function() {
                var $tab = $(this);
                $recipientType.val($tab.data('recipientType'));
            });
        });
    };

    $.fn.usersGrid = function(options) {
        var settings = $.extend({
            addRecipientUrl: ""
        }, options);

        return this.each(function() {
            var $this = $(this);

            $this.find('input.event-recipient').on('change', function() {
                var $this = $(this),
                    val = $this.val(),
                    checked = $this.prop('checked'),
                    url = settings.addRecipientUrl.replace('__ID__', val).replace('__REMOVE__', !checked? 1: 0);
                $this.parent().parent().toggleClass('info');
                $.ajax({url: url, method: 'post'});
            });
        });
    };

})(jQuery);