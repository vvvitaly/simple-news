<?php

use yii\helpers\ArrayHelper;


$base = require (__DIR__ . '/_web.php');
$env = require (__DIR__ . '/env_params.php');

$config = ArrayHelper::merge(
    $base,
    [
        'bootstrap' => [
            function() {
                $notifier = Yii::$app->notifier;
                $notifier->events = require (__DIR__ . '/_events.php');
                $notifier->listen();
            }
        ],
        'components' => [
            'request' => [
                'cookieValidationKey' => '8oKWksfkJdz3LxVNuixSI0a9wNaaYp9T',
            ],
            'db' => $env['db'],
            'authManager' => $env['authManager'],
            'queue' => $env['queue'],
            'notifier' => $env['notifier'],
        ],
        'params' => $env['params'],
    ]
);

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];
}

return $config;
