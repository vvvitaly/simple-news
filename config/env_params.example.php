<?php
/**
 *
 * User: psi
 * Date: 22.01.17
 * Time: 11:55
 */

use app\events\Notifier;
use app\events\transports\Email;
use app\queue\SyncQueue;
use UrbanIndo\Yii2\Queue\Queues\DbQueue;
use yii\rbac\DbManager;

return [
    'db' => [
        'class' => 'yii\db\Connection',
        'dsn' => '',
        'username' => '',
        'password' => '',
        'charset' => 'utf8',
    ],
    'db_test' => [
        'class' => 'yii\db\Connection',
        'dsn' => '',
        'username' => '',
        'password' => '',
        'charset' => 'utf8',
    ],
    'mailer' => [
        'class' => 'yii\swiftmailer\Mailer',
        'useFileTransport' => true,
    ],
    'authManager' => [
        'class' => DbManager::class,
        'defaultRoles' => ['guest'],
    ],
    'queue' => [
        'class' => DbQueue::class,
        'module' => 'jobs',
    ],
    'queue_test' => [
        'class' => SyncQueue::class,
        'module' => 'jobs',
    ],
    'notifier' => [
        'class' => Notifier::class,
        'queue' => 'queue',
        'transports' => [
            'email' => ['class' => Email::class],
        ],
    ],
    'params' => [
        'brand' => '',
        'loginDuration' => 3600*24*30,
        'newsPreviewsCount' => 20,
        'newsPreviewSize' => 250,
        'activationCodeLifetimeDays' => 3,
    ],
];