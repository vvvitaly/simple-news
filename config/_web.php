<?php
/**
 *
 * User: psi
 * Date: 22.01.17
 * Time: 12:23
 */

use app\events\Notifier;
use app\events\transports\Email;
use app\jobs\Module as JobsModule;

return [
    'id' => 'news.loc',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'homeUrl' => ['/news/index'],
    'defaultRoute' => 'news/index',
    'components' => [
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'assetManager' => [
            'linkAssets' => true,
            'appendTimestamp' => true,
        ],
    ],
    'modules' => [
        'jobs' => [
            'class' => JobsModule::class,
        ]
    ],
    'params' => require (__DIR__ . '/params.php'),
];