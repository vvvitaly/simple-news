<?php

return [
    'brand' => 'Simple News Portal',
    'emailFrom' => 'promo@news.dev',
    'loginDuration' => 3600*24*30,
    'newsPreviewsCount' => 20,
    'newsPreviewSize' => 250,
    'activationCodeLifetimeDays' => 3,
];
