<?php

use yii\helpers\ArrayHelper;


$base = require (__DIR__ . '/_console.php');
$env = require (__DIR__ . '/env_params.php');

$config = ArrayHelper::merge(
    $base,
    [
        'components' => [
            'db' => $env['db'],
            'authManager' => $env['authManager'],
            'queue' => $env['queue'],
            'mailer' => $env['mailer'],
            'notifier' => $env['notifier'],
        ],
        'params' => $env['params'],
    ]
);

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
