<?php
/**
 *
 * User: psi
 * Date: 22.01.17
 * Time: 12:05
 */

use app\events\Notifier;
use app\events\transports\Email;
use app\jobs\Module as JobsModule;
use UrbanIndo\Yii2\Queue\Console\Controller;

$env = require (__DIR__ . '/env_params.php');

return [
    'id' => 'news-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'scriptUrl' => '/index.php',
        ],
    ],
    'params' => require(__DIR__ . '/params.php'),
    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
            'templatePath' => '@app/tests/fixtures/templates',
            'fixtureDataPath' => '@app/tests/fixtures/data',
            'namespace' => 'app\tests\fixtures',
        ],
        'queue' => [
            'class' => Controller::class,
        ],
    ],
    'modules' => [
        'jobs' => [
            'class' => JobsModule::class,
        ]
    ],
];