<?php
/**
 *
 * User: psi
 * Date: 24.01.17
 * Time: 14:28
 */

use app\models\News;
use app\models\User;

/**
 * Events for notifier:
 * [class name, event name]
 */

return [
    [User::class, User::EVENT_SIGNUP_COMPLETE],
    [User::class, User::EVENT_RESET_PASSWORD],
    [News::class, News::EVENT_AFTER_INSERT],
];