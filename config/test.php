<?php

use yii\helpers\ArrayHelper;

$base = require (__DIR__ . '/_web.php');
$env = require (__DIR__ . '/env_params.php');

return ArrayHelper::merge(
    $base,
    [
        'bootstrap' => [
            function() {
                $notifier = Yii::$app->notifier;
                $notifier->events = require (__DIR__ . '/_events.php');
                $notifier->listen();
            }
        ],
        'components' => [
            'db' => $env['db_test'],
            'authManager' => $env['authManager'],
            'request' => [
                'cookieValidationKey' => 'test',
                'enableCsrfValidation' => false,
            ],
            'assetManager' => [
                'basePath' => __DIR__ . '/../web/assets',
            ],
            'queue' => $env['queue_test'],
            'notifier' => $env['notifier'],
        ],
        'params' => $env['params'],
    ]
);

