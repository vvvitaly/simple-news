<?php
/**
 *
 * User: psi
 * Date: 22.01.17
 * Time: 12:02
 */

use yii\helpers\ArrayHelper;

$base = require (__DIR__ . '/_console.php');
$env = require (__DIR__ . '/env_params.php');

return ArrayHelper::merge(
    $base,
    [
        'components' => [
            'db' => $env['db_test'],
            'authManager' => $env['authManager'],
            'queue' => $env['queue'],
        ],
        'params' => $env['params'],
    ]
);
