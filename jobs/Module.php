<?php

namespace app\jobs;

/**
 * jobs module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\jobs';
}
