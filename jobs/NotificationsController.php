<?php
/**
 *
 * User: psi
 * Date: 24.01.17
 * Time: 18:18
 */

namespace app\jobs;


use UrbanIndo\Yii2\Queue\Worker\Controller;

class NotificationsController extends Controller
{
    public function actionSend($id, $event, $sender)
    {
        \Yii::$app->notifier->send($id, $event, $sender);
        return true;
    }
}