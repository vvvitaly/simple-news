<?php
/**
 *
 * User: psi
 * Date: 22.01.17
 * Time: 11:10
 */

namespace app\commands;


use app\auth\Permissions;
use app\models\EventNotification;
use app\models\News;
use app\models\User;
use app\models\UserActivationCode;
use Faker\Factory;
use Faker\Generator;
use yii\base\Event;
use yii\console\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;

class InitController extends Controller
{
    const ADMIN_EMAIL = 'admin@news.dev';
    const USER_PASSWORD = 'test123';

    /**
     * @var bool Generate fake data
     */
    public $fake = false;

    /**
     * @var string Size of fake data (some, normal, many)
     */
    public $size = 'normal';

    private static $_fakeCounts = [
        User::class => [
            'some' => [5, 10],
            'normal' => [20, 50],
            'many' => [100, 300]
        ],
        News::class => [
            'some' => [30, 70],
            'normal' => [100, 200],
            'many' => [300, 500]
        ],
    ];

    /**
     * @var Generator
     */
    private $_faker;

    /**
     * @var boolean
     */
    public $emails;

    /**
     * @inheritdoc
     */
    public function options($actionID)
    {
        return array_merge(parent::options($actionID),
            $actionID === 'index'? ['fake', 'size', 'emails']: []);
    }


    public function actionIndex()
    {
        if (!User::find()->where(['email' => self::ADMIN_EMAIL])->exists()) {
            $this->createAdmin(self::ADMIN_EMAIL);
        }

        if ($this->fake) {
            $this->generateFakeData();
        }

        if ($this->emails) {
            $this->actionEmails();
        }
    }

    public function actionEmails()
    {
        $notifications = [
            [
                'target_class' => User::class,
                'target_event' => User::EVENT_SIGNUP_COMPLETE,
                'transport_name' => 'email',
                'recipient_type' => 'owner',
                'content_template' => json_encode([
                    'subject' => 'Activate your account',
                    'body' => 'Hi <b>{{sender.name}}</b>! Welcome to our super-duper news portal. Please activate your account <a href="{{sender.currentAccountActivationCode.link}}">here</a>'
                ])
            ],
            [
                'target_class' => User::class,
                'target_event' => User::EVENT_RESET_PASSWORD,
                'transport_name' => 'email',
                'recipient_type' => 'owner',
                'content_template' => json_encode([
                    'subject' => 'New password',
                    'body' => 'Hi <b>{{sender.name}}</b>! Change your password <a href="{{sender.currentResetPasswordCode.link}}">here</a>'
                ])
            ],
            [
                'target_class' => News::class,
                'target_event' => News::EVENT_AFTER_INSERT,
                'transport_name' => 'email',
                'recipient_type' => 'all',
                'content_template' => json_encode([
                    'subject' => 'New article were added!',
                    'body' => 'Hi! {{sender.user.name}} added new <a href="{{sender.link}}">article</a> "{{sender.title}}"'
                ])
            ],
        ];

        foreach ($notifications as $n) {
            (new EventNotification($n))->save();
        }
        $this->stdout('Done!');
    }

    /**
     * Create admin account
     * @param string $email
     * @return int
     */
    public function actionAdmin($email)
    {
        $this->createAdmin($email);
        return self::EXIT_CODE_NORMAL;
    }

    protected function createAdmin($email)
    {
        $this->stdout("Create admin account\n");
        $password = \Yii::$app->security->generateRandomString(6);
        $user = new User([
            'email' => $email,
            'password' => $password,
            'name' => 'ADMIN',
            'is_activated' => 1,
        ]);
        if (!$user->save()) {
            $this->stderr('Can not save user: ' . print_r($user->errors, true) . PHP_EOL, Console::FG_RED);
            return false;
        }
        $admin = \Yii::$app->authManager->getRole(Permissions::ROLE_ADMIN);
        \Yii::$app->authManager->assign($admin, $user->id);
        $this->stdout("Admin was created with password: $password\n");
        return true;
    }

    protected function generateFakeData()
    {
        $this->_faker = Factory::create();
        // users
        $count = $this->getFakeCount(User::class, $this->size);
        $passwordHash = \Yii::$app->security->generatePasswordHash(self::USER_PASSWORD);
        $auth = \Yii::$app->authManager;
        $roleAdmin = $auth->getRole(Permissions::ROLE_ADMIN);
        $roleModerator = $auth->getRole(Permissions::ROLE_MODERATOR);
        $roleUser = $auth->getRole(Permissions::ROLE_USER);
        $users = [];

        $this->stdout("Generate $count users with password " . self::USER_PASSWORD . " ...\n");

        for ($i = 0; $i < $count; $i++) {
            $u = new User([
                'email' => $this->_faker->email,
                'password_hash' => $passwordHash,
                'auth_key' => \Yii::$app->security->generateRandomString(),
                'name' => $this->_faker->name,
                'is_activated' => (int)$this->_faker->boolean(70),
            ]);
            $u->detachBehaviors();
            $u->created_at = $this->_faker->dateTimeBetween('-3 years', '-1 hour')->format('Y-m-d H:i:s');
            $u->save();
            $u->refresh();

            $users[] = $u;

            if ($this->_faker->boolean(35)) {
                $auth->assign($roleAdmin, $u->id);
            } elseif ($this->_faker->boolean(50)) {
                $auth->assign($roleModerator, $u->id);
            } else {
                $auth->assign($roleUser, $u->id);
            }

            $createdAt = new \DateTime($u->created_at);

            if (!$u->is_activated) {
                $uac = UserActivationCode::generateForUser($u, UserActivationCode::TYPE_ACTIVATE_ACCOUNT, null, false);
                $uac->created_at = $this->_faker->dateTimeBetween(
                    $createdAt->sub(new \DateInterval('P7D')), $u->created_at)->format('Y-m-d H:i:s');
                $uac->save();
            }

            if ($this->_faker->boolean(40)) {
                $uac = UserActivationCode::generateForUser($u, UserActivationCode::TYPE_RESET_PASSWORD, null, false);
                $uac->created_at = $this->_faker->dateTimeBetween(
                    $createdAt->sub(new \DateInterval('P7D')), $u->created_at)->format('Y-m-d H:i:s');
                $uac->save();
            }
        }

        // news
        $count = $this->getFakeCount(News::class, $this->size);

        $this->stdout("Generate $count news...\n");
        for ($i = 0; $i < $count; $i++) {
            $u = $this->_faker->randomElement($users);
            $n = new News([
                'user_id' => $u->id,
                'title' => $this->_faker->sentence($this->_faker->numberBetween(2, 5)),
                'body' => $this->_faker->realText($this->_faker->numberBetween(500, 3000)),
            ]);
            $n->detachBehaviors();
            $n->created_at = $this->_faker->dateTimeBetween($u->created_at)->format('Y-m-d H:i:s');
            $n->save();
        }

        $this->stdout("Done!\n", Console::FG_GREEN);
    }

    protected function getFakeCount($class, $type)
    {
        $range = self::$_fakeCounts[$class][$type];
        return $this->_faker->numberBetween($range[0], $range[1]);
    }

    /**
     * @param int $userId user id to assign
     * @param string $role role name
     * @return int
     */
    public function actionAssignRole($userId, $role)
    {
        $auth = \Yii::$app->authManager;

        if (!User::find()->where(['id' => $userId])->exists()) {
            $this->stderr("Wrong user\n", Console::FG_RED);
            return self::EXIT_CODE_ERROR;
        }

        $roleInstance = $auth->getRole($role);
        if (!$roleInstance) {
            $roles = implode(', ', ArrayHelper::getColumn($auth->getRoles(), 'name'));
            $this->stderr("Wrong role name. Use one of [$roles]\n", Console::FG_RED);
            return self::EXIT_CODE_ERROR;
        }

        if ($auth->getAssignment($role, $userId)) {
            $this->stderr("User alread has this role\n", Console::FG_RED);
            return self::EXIT_CODE_ERROR;
        }

        $auth->assign($roleInstance, $userId);
        $this->stdout('Done', Console::FG_GREEN);
        return self::EXIT_CODE_NORMAL;
    }
}