<?php
/**
 *
 * User: psi
 * Date: 19.01.17
 * Time: 22:57
 */

namespace app\models;


use yii\base\Event;

class ActivationEvent extends Event
{
    /**
     * @var UserActivationCode
     */
    public $activation;
}