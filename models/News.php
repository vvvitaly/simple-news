<?php

namespace app\models;

use app\events\NotifiableInterface;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\StringHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $created_at
 * @property integer $user_id
 * @property string $title
 * @property string $body
 *
 * @property string $preview read-only news content preview
 * @property User $user
 */
class News extends \yii\db\ActiveRecord implements NotifiableInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'title', 'body'], 'required'],
            [['created_at'], 'safe'],
            [['user_id'], 'integer'],
            [['body'], 'string', 'min' => 10],
            [['title'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'user_id' => 'Author',
            'title' => 'Title',
            'body' => 'Contents',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Get body preview
     * @param null|integer $limit
     * @return string
     */
    public function getPreview($limit=null)
    {
        if (!$this->body) {
            return '';
        }

        if (!$limit) {
            $limit = Yii::$app->params['newsPreviewSize'];
        }

        return StringHelper::truncate($this->body, $limit);
    }

    /**
     * @inheritdoc
     */
    public function getOwnerId()
    {
        return $this->user_id;
    }

    public function getLink()
    {
        return Url::to(['news/view', 'id' => $this->id]);
    }
}
