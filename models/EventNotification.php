<?php

namespace app\models;

use Yii;
use yii\base\InvalidParamException;
use yii\helpers\Inflector;
use yii\validators\Validator;

/**
 * This is the model class for table "events_notifications".
 *
 * @property integer $id
 * @property string $transport_name
 * @property string $target_class
 * @property string $target_event
 * @property string $recipient_type
 * @property string $recipient_value
 * @property string $content_template
 * @property integer $is_autoremove
 * @property string $recipient get recipient string
 */
class EventNotification extends \yii\db\ActiveRecord
{
    const RECIPIENTS_ALL = 'all';
    const RECIPIENTS_GROUP = 'group';
    const RECIPIENTS_ROLE = 'role';
    const RECIPIENTS_OWNER = 'owner';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'events_notifications';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['transport_name', 'target_class', 'target_event', 'recipient_type'], 'required'],
            [['recipient_type', 'content_template'], 'string'],
            [
                ['recipient_type'],
                'in',
                'range' => [self::RECIPIENTS_ALL, self::RECIPIENTS_ROLE, self::RECIPIENTS_GROUP, self::RECIPIENTS_OWNER]
            ],
            [['transport_name', 'target_event'], 'string', 'max' => 64],
            [['target_class'], 'string', 'max' => 128],
            [['recipient_value'], 'string', 'max' => 255],
            ['recipient_value', EventNotificationRecipientValidator::class, 'skipOnEmpty' => false],
            ['is_autoremove', 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'target_class' => 'Initiator',
            'target_event' => 'Event',
            'transport_name' => 'Transport',
            'recipient_type' => 'Recipient',
            'recipient_value' => 'Recipient Role',
            'content_template' => 'Template',
            'is_autoremove' => 'Remove after send',
            // displayed* attributes
            'displayedTarget' => 'Initiator',
            'displayedEvent' => 'Event',
            'displayedTransport' => 'Transport',
        ];
    }

    public function getDisplayedTarget()
    {
        return $this->target_class;
    }

    public function getDisplayedEvent()
    {
        return Inflector::titleize($this->target_event);
    }

    public function getDisplayedTransport()
    {
        return Inflector::titleize($this->transport_name);
    }

    public function recipientTypes()
    {
        return [
            self::RECIPIENTS_ALL => 'All',
            self::RECIPIENTS_GROUP => 'Group Users',
            self::RECIPIENTS_ROLE => 'Role ' . ($this->recipient_value? ' (' . $this->recipient_value . ')': ''),
            self::RECIPIENTS_OWNER => 'Owner',
        ];
    }

    /**
     * Add new recipient
     * @param int $userId
     * @return EventNotificationRecipient
     * @throws InvalidParamException
     */
    public function addRecipient($userId)
    {
        if ($this->recipient_type !== self::RECIPIENTS_GROUP) {
            throw new InvalidParamException('Can not add recipients for ' . $this->recipient_type);
        }

        $recipient = new EventNotificationRecipient(['notification_id' => $this->id, 'user_id' => $userId]);
        $recipient->save();
        return $recipient;
    }

    /**
     * Remove recipient
     * @param int $userId
     * @return integer
     * @throws InvalidParamException
     */
    public function removeRecipient($userId)
    {
        if ($this->recipient_type !== self::RECIPIENTS_GROUP) {
            throw new InvalidParamException('Can not add recipients for ' . $this->recipient_type);
        }

        $recipient = EventNotificationRecipient::findOne(['notification_id' => $this->id, 'user_id' => $userId]);
        return $recipient->delete();
    }

    /**
     * @return string
     */
    public function getRecipient()
    {
        return $this->recipient_type? $this->recipientTypes()[$this->recipient_type]: '';
    }

    /**
     * Check is_autoremove: if true - remove current model
     * @return bool true if removed
     */
    public function autoremove()
    {
        if (!$this->is_autoremove) {
            return false;
        }
        return $this->delete() > 0;
    }

    /**
     * Create a clone of current notification to send manually (new records, reset recipient & autoremove)
     * @return EventNotification
     */
    public function createManualNotification()
    {
        $model = new static([
            'transport_name' => $this->transport_name,
            'target_class' => $this->target_class,
            'target_event' => $this->target_event,
            'recipient_type' => null,
            'recipient_value' => null,
            'content_template' => $this->content_template,
            'is_autoremove' => 1,
        ]);
        return $model;
    }
}
