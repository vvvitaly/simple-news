<?php
/**
 *
 * User: psi
 * Date: 17.01.17
 * Time: 20:00
 */

namespace app\models;


use yii\base\Model;


/**
 * Class SignupForm
 * @package app\models
 * @property string $name
 */
class SignupForm extends Model
{
    public $email;
    public $password;
    public $password_repeat;
    private $_name;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['email', 'password', 'password_repeat'], 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => User::class, 'targetAttribute' => 'email'],
            ['email', 'string', 'max' => 64],
            ['name', 'string', 'max' => 128],
            ['password', 'string', 'min' => 6],
            ['password', 'compare', 'compareAttribute' => 'password_repeat'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'Email',
            'password' => 'Password',
            'password_repeat' => 'Repeat Password',
            'name' => 'Display Name',
        ];
    }

    /**
     * Set $name property
     * @param string|null $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * Get name. If empty returns part of email before @.
     * @return string
     */
    public function getName()
    {
        return $this->_name?: explode('@', $this->email)[0];
    }

    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = $this->createUser();
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->name = $this->getName();

        return $user->signup() ? $user : null;
    }

    /**
     * Create new user model instance
     * @return User
     */
    public function createUser()
    {
        return new User();
    }
}