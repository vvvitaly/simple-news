<?php
/**
 *
 * User: psi
 * Date: 19.01.17
 * Time: 23:38
 */

namespace app\models;


use yii\base\Model;
use yii\db\ActiveQuery;


/**
 * Class BaseActivationModel
 * @package app\models
 *
 * Base model class for models needed activation
 */
abstract class BaseActivationModel extends Model
{
    /**
     * @var string Activation code
     */
    public $code;

    /**
     * @var User
     */
    private $_user = false;

    /**
     * @var UserActivationCode
     */
    private $_activation = false;

    /**
     * @return string Activation type (@see UserActivationCode TYPE_* constants)
     */
    abstract public function getType();

    /**
     * Additional query for code validation
     * @param ActiveQuery $query
     */
    public function filterCodeQuery($query)
    {
    }

    public function rules()
    {
        return [
            [
                ['code'], 'exist',
                'targetClass' => UserActivationCode::class, 'targetAttribute' => 'code',
                'filter' => function($query) {
                    $query->andWhere(['type' => $this->getType()]);
                    $this->filterCodeQuery($query);
                },
                'message' => 'Wrong activation code.',
            ],
            [['code'], 'validateCode'],
        ];
    }

    public function setActivationModel(UserActivationCode $activation)
    {
        $this->_activation = $activation;
        $this->code = $activation->code;
    }

    /**
     * @return UserActivationCode
     */
    public function getActivationModel()
    {
        if ($this->_activation === false) {
            $query = UserActivationCode::find();
            $query->andWhere(['code' => $this->code]);
            $this->filterCodeQuery($query);
            $this->_activation = $query->one();
        }
        return $this->_activation;
    }

    public function validateCode($attribute, $params)
    {
        $activation = $this->getActivationModel();
        if (!$activation || !$activation->isValid()) {
            $this->addError($attribute, 'Wrong activation code.');
        }
    }
}