<?php

namespace app\models;

use app\auth\Permissions;
use app\events\NotifiableInterface;
use Yii;
use yii\base\Event;
use yii\base\UserException;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\StringHelper;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $created_at
 * @property string $email
 * @property string $password_hash
 * @property string $auth_key
 * @property string $name
 * @property string $shortName
 * @property string $password
 * @property boolean $is_activated
 * @property boolean $is_allow_emails
 * @property EventNotification $notifications[]
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface, NotifiableInterface
{
    /**
     * @event Successful registration
     */
    const EVENT_SIGNUP_COMPLETE = 'signupComplete';

    /**
     * @event Account activated
     */
    const EVENT_ACTIVATED = 'activated';

    /**
     * @event Request password reset
     */
    const EVENT_RESET_PASSWORD = 'resetPassword';

    /**
     * @event Password changed
     */
    const EVENT_CHANGE_PASSWORD = 'changePassword';

    const SCENARIO_EDIT = 'edit';

    /**
     * @var string role name
     */
    public $role;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at'], 'safe'],
            [['email'], 'required'],
            [['email', 'name', 'role'], 'required', 'on' => self::SCENARIO_EDIT],
            ['role', function($attribute, $params) {
                $role = Yii::$app->authManager->getRole($this->$attribute);
                if (!$role) {
                    $this->addError($attribute, "Role {$this->role} does not exists.");
                }
            }, 'on' => self::SCENARIO_EDIT],
            [['password_hash'], 'required', 'except' => self::SCENARIO_EDIT],
            [['email'], 'string', 'max' => 64],
            [['email'], 'email'],
            [['email'], 'unique'],
            [['password_hash'], 'string', 'max' => 60],
            [['name'], 'string', 'max' => 128],
            [['is_activated'], 'boolean'],
            [['is_activated'], 'default', 'value' => 0],
            [['is_allow_emails'], 'default', 'value' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return array_merge(parent::scenarios(), [
            self::SCENARIO_EDIT => ['email', 'name', 'role'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'email' => 'Email',
            'password_hash' => 'Password',
            'auth_key' => 'Auth Key',
            'name' => 'Name',
            'is_activated' => 'Activated',
            'is_allow_emails' => 'Allow Email Notifications',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function getNotifications()
    {
        return $this->hasMany(EventNotification::class, ['id' => 'notification_id'])
            ->viaTable(EventNotificationRecipient::tableName(), ['user_id' => 'id'])
            ->indexBy('id');
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new \RuntimeException('Does not implemented.');
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Generate random key for auth_key attribute
     */
    public function generateAuthKey()
    {
        $this->auth_key = \Yii::$app->security->generateRandomString();
    }

    /**
     * Compare password & hash
     * @param string $password raw password
     * @return bool
     */
    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password_hash);
    }

    /**
     * Calculate password hash
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->generateAuthKey();
            }
            return true;
        }
        return false;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($this->scenario === self::SCENARIO_EDIT) {
            $this->assignRole($this->role);
        } elseif($insert) {
            $this->assignRole(Permissions::ROLE_USER);
        }
    }

    /**
     * Short length user name
     * @return string
     */
    public function getShortName()
    {
        return $this->name? StringHelper::truncate($this->name, 15): '';
    }

    public function assignRole($role)
    {
        $auth = Yii::$app->authManager;
        $role = $auth->getRole($role);
        $roles = $auth->getRolesByUser($this->id);
        if(!isset($roles[$role->name])) {
            foreach ($roles as $r) {
                $auth->revoke($r, $this->id);
            }
            $auth->assign($role, $this->id);
        }
    }

    /**
     * Signup new user. Triggers event EVENT_SIGNUP_COMPLETE. Throws an exception if record is not new.
     * @return boolean
     * @throws SignupException
     */
    public function signup()
    {
        if (!$this->isNewRecord) {
            throw new SignupException('User already registered.');
        }

        if ($this->save()) {
            UserActivationCode::generateForUser($this, UserActivationCode::TYPE_ACTIVATE_ACCOUNT);
            $this->trigger(static::EVENT_SIGNUP_COMPLETE);
            return true;
        }
        return false;
    }

    public function getAccountActivationCodes()
    {
        return $this->hasMany(UserActivationCode::class, ['user_id' => 'id'])
            ->where('type=:type', [':type' => UserActivationCode::TYPE_ACTIVATE_ACCOUNT]);
    }

    public function getResetPasswordCodes()
    {
        return $this->hasMany(UserActivationCode::class, ['user_id' => 'id'])
            ->where('type=:type', [':type' => UserActivationCode::TYPE_RESET_PASSWORD]);
    }

    /**
     * @return UserActivationCode
     */
    public function getCurrentAccountActivationCode()
    {
        return $this->getAccountActivationCodes()->andWhere(['is_used' => 0])->one();
    }

    /**
     * @return UserActivationCode
     */
    public function getCurrentResetPasswordCode()
    {
        return $this->getAccountActivationCodes()->andWhere(['is_used' => 0])->one();
    }

    protected function checkActivation(UserActivationCode $activationCode, $expectedType)
    {
        return $activationCode->isValid() &&
            $activationCode->type === $expectedType &&
            $activationCode->user_id == $this->id;
    }

    /**
     * Activate account
     * @param UserActivationCode $activationCode
     * @return boolean
     * @throws \Exception
     */
    public function activate(UserActivationCode $activationCode)
    {
        if ($this->is_activated) {
            throw new UserException('Account already activated.');
        }

        if (!$this->checkActivation($activationCode, UserActivationCode::TYPE_ACTIVATE_ACCOUNT)) {
            throw new UserException('Activation code is not valid.');
        }

        $this->is_activated = true;
        if ($this->save()) {
            $activationCode->apply();
            $this->trigger(self::EVENT_ACTIVATED);
        }

        return true;
    }

    /**
     * Change password
     * @param string $password
     * @param UserActivationCode $activationCode
     * @return boolean
     * @throws \Exception
     */
    public function changePassword($password, UserActivationCode $activationCode)
    {
        if (!$this->checkActivation($activationCode, UserActivationCode::TYPE_RESET_PASSWORD)) {
            throw new UserException('Activation code is not valid.');
        }

        $activationCode->apply();
        $this->setPassword($password);
        $saved = $this->save();
        if ($saved) {
            $this->trigger(self::EVENT_CHANGE_PASSWORD);
            $activationCode->apply();
        }
        return $saved;
    }

    /**
     * Reset user password
     * @return UserActivationCode
     */
    public function resetPassword()
    {
        $activation = UserActivationCode::generateForUser($this, UserActivationCode::TYPE_RESET_PASSWORD);
        if ($activation->id) {
            $activation->refresh();
            $this->trigger(self::EVENT_RESET_PASSWORD, new ActivationEvent(['activation' => $activation]));
        }
        return $activation;
    }

    /**
     * @inheritdoc
     */
    public function getOwnerId()
    {
        return $this->id;
    }
}
