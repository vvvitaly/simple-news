<?php
/**
 *
 * User: psi
 * Date: 21.01.17
 * Time: 18:29
 */

namespace app\models;


use yii\data\ActiveDataProvider;

class UserSearch extends User
{
    public function rules()
    {
        return [
            [['id', 'is_activated'], 'integer'],
            [['email', 'name'], 'safe'],
        ];
    }

    public function search($params)
    {
        $query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query
            ->andFilterWhere(['id' => $this->id])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['is_activated' => $this->is_activated])
            ->orderBy(['created_at' => SORT_DESC]);

        return $dataProvider;
    }
}