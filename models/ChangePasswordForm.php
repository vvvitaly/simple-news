<?php
/**
 *
 * User: psi
 * Date: 19.01.17
 * Time: 23:37
 */

namespace app\models;


class ChangePasswordForm extends BaseActivationModel
{
    public $password;
    public $password_repeat;

    /**
     * @var User
     */
    private $_user;

    public function getType()
    {
        return UserActivationCode::TYPE_RESET_PASSWORD;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['password', 'password_repeat'], 'required'],
            ['password', 'string', 'min' => 6],
            ['password', 'compare', 'compareAttribute' => 'password_repeat'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'password' => 'Password',
            'password_repeat' => 'Repeat Password',
        ];
    }

    /**
     * @return boolean
     */
    public function changePassword()
    {
        if (!$this->validate()) {
            return false;
        }

        return $this->getActivationModel()->user->changePassword($this->password, $this->getActivationModel());
    }
}