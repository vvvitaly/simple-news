<?php
/**
 *
 * User: psi
 * Date: 24.01.17
 * Time: 21:24
 */

namespace app\models;


use yii\helpers\Html;
use yii\helpers\Json;
use yii\validators\Validator;

class EventNotificationRecipientValidator extends Validator
{
    public function init()
    {
        parent::init();
        $this->message = 'Recipient role can not be blank.';
    }

    public function validateAttribute($model, $attribute)
    {
        if ($model->recipient_type === EventNotification::RECIPIENTS_ROLE && empty($model->$attribute)) {
            $model->addError($attribute,$this->message);
        }
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        $recipientTypeAttr = Html::getInputId($model, 'recipient_type');
        $typeRole = EventNotification::RECIPIENTS_ROLE;
        $message = Json::encode($this->message);

        return <<<JS
recipientType = $('#{$recipientTypeAttr}').val();
if (recipientType === "{$typeRole}" && yii.validation.isEmpty(value)) {
    messages.push($message);
}
JS;
    }
}