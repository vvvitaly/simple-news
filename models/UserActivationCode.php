<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;

/**
 * This is the model class for table "users_activation_codes".
 *
 * @property integer $id
 * @property string $created_at
 * @property string $active_to
 * @property string $type
 * @property integer $user_id
 * @property string $code
 * @property boolean $is_used
 *
 * @property User $user
 */
class UserActivationCode extends \yii\db\ActiveRecord
{
    const TYPE_ACTIVATE_ACCOUNT = 'activate_account';
    const TYPE_RESET_PASSWORD = 'reset_password';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_activation_codes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['active_to', 'type', 'user_id', 'code'], 'required'],
            [['created_at', 'active_to'], 'safe'],
            [['type'], 'string'],
            [['type'], 'in', 'range' => [self::TYPE_ACTIVATE_ACCOUNT, self::TYPE_RESET_PASSWORD]],
            [['user_id'], 'integer'],
            [['code'], 'string', 'max' => 32],
            [['user_id', 'type', 'code'], 'unique', 'targetAttribute' => ['type', 'code'], 'message' => 'The combination of Type and Code has already been taken.'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            [['is_used'], 'boolean'],
            [['is_used'], 'default', 'value' => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'active_to' => 'Active To',
            'type' => 'Type',
            'user_id' => 'User ID',
            'code' => 'Code',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * Generate new UserActivationCode instance with random code
     * @param User $user
     * @param string $type
     * @param null|integer $lifetimeDays code lifetime in days
     * @param boolean $save save new instance immediately
     * @return static
     */
    public static function generateForUser(User $user, $type, $lifetimeDays=null, $save=true)
    {
        if ($lifetimeDays === null) {
            $lifetimeDays = Yii::$app->params['activationCodeLifetimeDays'];
        }

        static::updateAll(
            ['is_used' => 1],
            [
                'type' => $type,
                'user_id' => $user->id,
                'is_used' => 0
            ]
        );

        $activation = new static([
            'active_to' => new Expression('NOW() + INTERVAL :days DAY', [':days' => $lifetimeDays]),
            'type' => $type,
            'user_id' => $user->id,
            'code' => Yii::$app->security->generateRandomString(32),
        ]);
        if ($save) {
            $activation->save();
        }
        return $activation;
    }

    /**
     * Apply activation code
     * @return boolean
     */
    public function apply()
    {
        $this->is_used = true;
        return $this->save();
    }

    /**
     * Check if activation is fresh & not used.
     * @return boolean
     */
    public function isValid()
    {
        return $this->active_to && strtotime($this->active_to) >= time() &&
            $this->is_used !== null && !$this->is_used;
    }

    public function getLink()
    {
        $routes = [
            self::TYPE_ACTIVATE_ACCOUNT => 'site/activation',
            self::TYPE_RESET_PASSWORD => 'profile/change-password',
        ];
        $url = [$routes[$this->type], 'code' => $this->code];
        return Url::to($url);
    }
}
