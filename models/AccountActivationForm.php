<?php
/**
 *
 * User: psi
 * Date: 19.01.17
 * Time: 9:00
 */

namespace app\models;


class AccountActivationForm extends BaseActivationModel
{
    /**
     * @inheritdoc
     */
    public function getType()
    {
        return UserActivationCode::TYPE_ACTIVATE_ACCOUNT;
    }

    /**
     * @inheritdoc
     */
    public function validateCode($attribute, $params)
    {
        $user = $this->getActivationModel()->user;
        if ($user && $user->is_activated) {
            $this->addError('code', 'User already activated.');
            return;
        }
        parent::validateCode($attribute, $params);
    }

    /**
     * Activate user with activation code
     * @return bool
     */
    public function activate()
    {
        if (!$this->validate()) {
            return false;
        }

        return $this->getActivationModel()->user->activate($this->getActivationModel());
    }
}