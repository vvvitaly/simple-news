<?php
/**
 *
 * User: psi
 * Date: 22.01.17
 * Time: 11:18
 */

namespace app\auth;


class Permissions
{
    const VIEW_NEWS_PREVIEW = 'viewNewsPreview';
    const VIEW_NEWS_CONTENTS = 'viewNewsContents';
    const EDIT_NEWS_CONTENTS = 'editNewsContents';
    const CREATE_NEWS = 'createNews';

    const REMOVE_NEWS = 'removeNews';
    const VIEW_USERS = 'viewUsers';
    const EDIT_USER = 'editUser';
    const CREATE_USER = 'createUser';
    const REMOVE_USER = 'removeUser';

    const CREATE_NOTIFICATION = 'createNotificaiton';
    const EDIT_NOTIFICATION = 'editNotificaiton';
    const VIEW_NOTIFICATIONS = 'viewNotificaitons';
    const REMOVE_NOTIFICATION = 'removeNotificaitons';
    const SEND_NOTIFICATION = 'sendNotifications';

    const ROLE_GUEST = 'guest';
    const ROLE_USER = 'user';
    const ROLE_MODERATOR = 'moderator';
    const ROLE_ADMIN = 'admin';
}