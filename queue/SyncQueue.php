<?php
/**
 *
 * User: psi
 * Date: 22.01.17
 * Time: 19:41
 */

namespace app\queue;


use UrbanIndo\Yii2\Queue\Job;
use UrbanIndo\Yii2\Queue\Queue;


/**
 * Dummy queue for testing purpose. Tasks are processed just after added.
 * Class SyncQueue
 * @package app\queue
 */
class SyncQueue extends Queue
{
    /**
     * @inheritdoc
     */
    protected function postJob(Job $job)
    {
        $this->run($job);
        return true;
    }

    /**
     * @inheritdoc
     */
    protected function fetchJob()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    protected function deleteJob(Job $job)
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    protected function releaseJob(Job $job)
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function getSize()
    {
        return 0;
    }

    /**
     * @inheritdoc
     */
    public function purge()
    {
        return false;
    }
}