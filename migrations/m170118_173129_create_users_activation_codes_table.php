<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users_activation_codes`.
 * Has foreign keys to the tables:
 *
 * - `user`
 */
class m170118_173129_create_users_activation_codes_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users_activation_codes', [
            'id' => $this->primaryKey(),
            'created_at' => $this->datetime()->notNull(),
            'active_to' => $this->datetime()->notNull(),
            'type' => "ENUM('activate_account', 'reset_password') NOT NULL",
            'user_id' => $this->integer()->notNull(),
            'code' => $this->string(32)->notNull(),
            'is_used' => $this->boolean()->notNull()->defaultValue(0),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-users_activation_codes-user_id',
            'users_activation_codes',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-users_activation_codes-user_id',
            'users_activation_codes',
            'user_id',
            'users',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'uc-users_activation_codes-user_id-type-code',
            'users_activation_codes',
            ['user_id', 'type', 'code'],
            true
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropIndex('uc-users_activation_codes-user_id-type-code', 'users_activation_codes');

        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-users_activation_codes-user_id',
            'users_activation_codes'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-users_activation_codes-user_id',
            'users_activation_codes'
        );

        $this->dropTable('users_activation_codes');
    }
}
