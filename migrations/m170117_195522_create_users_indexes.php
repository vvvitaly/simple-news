<?php

use yii\db\Migration;

class m170117_195522_create_users_indexes extends Migration
{
    public function up()
    {
        $this->createIndex('uc_users_email', 'users', 'email', true);
        $this->createIndex('ix_users_created_at', 'users', 'created_at');
    }

    public function down()
    {
        $this->dropIndex('uc_users_email', 'users');
        $this->dropIndex('ix_users_created_at', 'users');
    }
}
