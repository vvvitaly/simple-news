<?php

use yii\db\Migration;

class m170123_193712_add_events_notifications_recipient_type extends Migration
{
    public function up()
    {
        $this->alterColumn(
            'events_notifications',
            'recipient_type',
            "ENUM('all', 'role', 'group', 'owner') NOT NULL"
        );
    }

    public function down()
    {
        $this->alterColumn(
            'events_notifications',
            'recipient_type',
            "ENUM('all', 'role', 'group') NOT NULL"
        );
    }
}
