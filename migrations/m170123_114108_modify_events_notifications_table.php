<?php

use yii\db\Migration;

class m170123_114108_modify_events_notifications_table extends Migration
{
    public function up()
    {
        $this->addColumn('events_notifications', 'recipient_value', $this->string()->null()->after('recipient_type'));
        $this->renameColumn('events_notifications', 'driver_name', 'transport_name');
    }

    public function down()
    {
        $this->renameColumn('events_notifications', 'transport_name', 'driver_name');
        $this->dropColumn('events_notifications', 'recipient_value');
    }
}
