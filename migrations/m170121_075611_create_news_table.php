<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news`.
 * Has foreign keys to the tables:
 *
 * - `users`
 */
class m170121_075611_create_news_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('news', [
            'id' => $this->primaryKey(),
            'created_at' => $this->datetime()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'title' => $this->string()->notNull(),
            'body' => $this->text(),
        ]);

        // creates index for column `created_at`
        $this->createIndex(
            'idx-news-created_at',
            'news',
            'user_id'
        );

        // creates index for column `user_id`
        $this->createIndex(
            'idx-news-user_id',
            'news',
            'created_at'
        );

        // add foreign key for table `users`
        $this->addForeignKey(
            'fk-news-user_id',
            'news',
            'user_id',
            'users',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `users`
        $this->dropForeignKey(
            'fk-news-user_id',
            'news'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-news-user_id',
            'news'
        );

        // drops index for column `created_at`
        $this->dropIndex(
            'idx-news-created_at',
            'news'
        );

        $this->dropTable('news');
    }
}
