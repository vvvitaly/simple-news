<?php

use app\auth\Permissions;

require (Yii::getAlias('@yii/rbac/migrations/m140506_102106_rbac_init.php'));


class m170122_133326_init_rbac extends m140506_102106_rbac_init
{
    public function up()
    {
        parent::up();

        $auth = \Yii::$app->authManager;

        // permissions
        $viewNewsPreview = $auth->createPermission(Permissions::VIEW_NEWS_PREVIEW);
        $viewNewsPreview->description = "View news contents preview";
        $auth->add($viewNewsPreview);

        $viewNewsContents = $auth->createPermission(Permissions::VIEW_NEWS_CONTENTS);
        $viewNewsContents->description = "View news full contents";
        $auth->add($viewNewsContents);

        $editNewsContents = $auth->createPermission(Permissions::EDIT_NEWS_CONTENTS);
        $editNewsContents->description = "Edit artice";
        $auth->add($editNewsContents);

        $createNews = $auth->createPermission(Permissions::CREATE_NEWS);
        $createNews->description = "Add articles";
        $auth->add($createNews);

        $removeNews = $auth->createPermission(Permissions::REMOVE_NEWS);
        $removeNews->description = "Remove articles";
        $auth->add($removeNews);

        $viewUsers = $auth->createPermission(Permissions::VIEW_USERS);
        $viewUsers->description = "View users";
        $auth->add($viewUsers);

        $editUsers = $auth->createPermission(Permissions::EDIT_USER);
        $editUsers->description = "Edit users data";
        $auth->add($editUsers);

        $createUsers = $auth->createPermission(Permissions::CREATE_USER);
        $createUsers->description = "Register new users";
        $auth->add($createUsers);

        $removeUsers = $auth->createPermission(Permissions::REMOVE_USER);
        $removeUsers->description = "Remove users";
        $auth->add($removeUsers);

        // roles
        $anon = $auth->createRole(Permissions::ROLE_GUEST);
        $auth->add($anon);
        $auth->addChild($anon, $viewNewsPreview);

        $user = $auth->createRole(Permissions::ROLE_USER);
        $auth->add($user);
        $auth->addChild($user, $viewNewsContents);
        $auth->addChild($user, $anon);

        $moder = $auth->createRole(Permissions::ROLE_MODERATOR);
        $auth->add($moder);
        $auth->addChild($moder, $editNewsContents);
        $auth->addChild($moder, $createNews);
        $auth->addChild($moder, $removeNews);
        $auth->addChild($moder, $user);

        $admin = $auth->createRole(Permissions::ROLE_ADMIN);
        $auth->add($admin);
        $auth->addChild($admin, $viewUsers);
        $auth->addChild($admin, $editUsers);
        $auth->addChild($admin, $createUsers);
        $auth->addChild($admin, $removeUsers);
        $auth->addChild($admin, $moder);
    }

    public function down()
    {
        parent::down();
    }
}
