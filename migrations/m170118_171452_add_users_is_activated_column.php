<?php

use yii\db\Migration;

class m170118_171452_add_users_is_activated_column extends Migration
{
    public function up()
    {
        $this->addColumn('users', 'is_activated', $this->boolean()->notNull()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('users', 'is_activated');
    }
}
