<?php

use yii\db\Migration;

/**
 * Handles the creation of table `events_routes`.
 */
class m170122_194559_create_events_routes_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('events_routes', [
            'id' => $this->primaryKey(),
            'driver_name' => $this->string(64)->notNull(),
            'target_class' => $this->string(128)->notNull(),
            'target_event' => $this->string(64)->notNull(),
        ]);

        $this->createIndex('idx-target_class-target_event', 'events_routes', ['target_class', 'target_event']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropIndex('idx-target_class-target_event', 'events_routes');
        $this->dropTable('events_routes');
    }
}
