<?php

use yii\db\Migration;

class m170124_205325_add_columns_users extends Migration
{
    public function up()
    {
        $this->addColumn('users', 'is_allow_emails', $this->boolean()->notNull()->defaultValue(1));
    }

    public function down()
    {
        $this->dropColumn('users', 'is_allow_emails');
    }
}
