<?php

use yii\db\Migration;

class m170123_031647_add_events_notifications_columns extends Migration
{
    public function up()
    {
        $this->addColumn(
            'events_notifications',
            'recipient_type',
            "ENUM('all', 'role', 'group') NOT NULL"
        );

        $this->addColumn(
            'events_notifications',
            'content_template',
            $this->text()
        );
    }

    public function down()
    {
        $this->dropColumn('events_notifications', 'recipient_type');
        $this->dropColumn('events_notifications', 'content_template');
    }
}
