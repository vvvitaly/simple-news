<?php

use yii\db\Migration;

class m170124_155045_add_events_notifications_columns extends Migration
{
    public function up()
    {
        $this->addColumn('events_notifications', 'is_autoremove', $this->boolean()->notNull()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('events_notifications', 'is_autoremove');
    }
}
