<?php

use yii\db\Migration;

/**
 * Handles the creation of table `events_notifications_recipients`.
 */
class m170123_200315_create_events_notifications_recipients_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('events_notifications_recipients', [
            'notification_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey(
            'pk-notification_id-user_id',
            'events_notifications_recipients',
            ['notification_id', 'user_id']
        );

        $this->addForeignKey(
            'fk-events_notifications_recipients-notification_id',
            'events_notifications_recipients',
            'notification_id',
            'events_notifications',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-events_notifications_recipients-user_id',
            'events_notifications_recipients',
            'user_id',
            'users',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-events_notifications_recipients-user_id', 'events_notifications_recipients');
        $this->dropForeignKey('fk-events_notifications_recipients-notification_id', 'events_notifications_recipients');
        $this->dropTable('events_notifications_recipients');
    }
}
