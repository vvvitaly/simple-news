<?php

use yii\db\Migration;

class m170123_031512_rename_events_routes_table extends Migration
{
    public function up()
    {
        $this->renameTable('events_routes', 'events_notifications');
    }

    public function down()
    {
        $this->renameTable('events_notifications', 'events_routes');
    }
}
