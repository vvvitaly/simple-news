<?php

use app\auth\Permissions;
use yii\db\Migration;

class m170123_210625_rbac_add_events_permissions extends Migration
{
    public function up()
    {
        $auth = \Yii::$app->authManager;

        $admin = $auth->getRole(Permissions::ROLE_ADMIN);

        $perm = $auth->createPermission(Permissions::VIEW_NOTIFICATIONS);
        $perm->description = "View events notifications";
        $auth->add($perm);
        $auth->addChild($admin, $perm);

        $perm = $auth->createPermission(Permissions::EDIT_NOTIFICATION);
        $perm->description = "Edit events notifications";
        $auth->add($perm);
        $auth->addChild($admin, $perm);

        $perm = $auth->createPermission(Permissions::CREATE_NOTIFICATION);
        $perm->description = "Create events notifications";
        $auth->add($perm);
        $auth->addChild($admin, $perm);

        $perm = $auth->createPermission(Permissions::REMOVE_NOTIFICATION);
        $perm->description = "Remove events notifications";
        $auth->add($perm);
        $auth->addChild($admin, $perm);

        $perm = $auth->createPermission(Permissions::SEND_NOTIFICATION);
        $perm->description = "Send events notifications";
        $auth->add($perm);
        $auth->addChild($admin, $perm);
    }

    public function down()
    {
        $auth = \Yii::$app->authManager;
        $admin = $auth->getRole(Permissions::ROLE_ADMIN);

        $auth->removeChild($admin, $auth->getPermission(Permissions::VIEW_NOTIFICATIONS));
        $auth->removeChild($admin, $auth->getPermission(Permissions::EDIT_NOTIFICATION));
        $auth->removeChild($admin, $auth->getPermission(Permissions::CREATE_NOTIFICATION));
        $auth->removeChild($admin, $auth->getPermission(Permissions::REMOVE_NOTIFICATION));
        $auth->removeChild($admin, $auth->getPermission(Permissions::SEND_NOTIFICATION));

        $auth->remove($auth->getPermission(Permissions::VIEW_NOTIFICATIONS));
        $auth->remove($auth->getPermission(Permissions::EDIT_NOTIFICATION));
        $auth->remove($auth->getPermission(Permissions::CREATE_NOTIFICATION));
        $auth->remove($auth->getPermission(Permissions::REMOVE_NOTIFICATION));
        $auth->remove($auth->getPermission(Permissions::SEND_NOTIFICATION));
    }
}
