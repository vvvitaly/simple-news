<?php

use yii\db\Migration;

/**
 * Handles the creation of table `queue`.
 */
class m170122_163424_create_queue_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('queue', [
            'id' => $this->bigPrimaryKey(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'timestamp' => $this->dateTime()->notNull()->defaultExpression('NOW()'),
            'data' => $this->binary()
        ]);

        $this->createIndex('idx-status', 'queue', ['status']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropIndex('idx-status', 'queue');
        $this->dropTable('queue');
    }
}
