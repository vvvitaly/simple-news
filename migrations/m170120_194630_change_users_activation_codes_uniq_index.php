<?php

use yii\db\Migration;

class m170120_194630_change_users_activation_codes_uniq_index extends Migration
{
    public function up()
    {
        $this->dropIndex('uc-users_activation_codes-user_id-type-code', 'users_activation_codes');
        $this->createIndex(
            'uc-users_activation_codes-type-code',
            'users_activation_codes',
            ['type', 'code'],
            true
        );
    }

    public function down()
    {
        $this->dropIndex('uc-users_activation_codes-type-code', 'users_activation_codes');
        $this->createIndex(
            'uc-users_activation_codes-user_id-type-code',
            'users_activation_codes',
            ['user_id', 'type', 'code'],
            true
        );
    }
}
