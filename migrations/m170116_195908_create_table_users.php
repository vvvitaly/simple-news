<?php

use yii\db\Migration;


class m170116_195908_create_table_users extends Migration
{
    public function up()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'created_at' => $this->dateTime()->notNull()->defaultExpression('NOW()'),
            'email' => $this->string(64)->notNull(),
            'password_hash' => $this->string(60)->notNull(),
            'auth_key' => $this->string(32)->notNull(),
            'name' => $this->string('128')->null(),
        ], 'ENGINE=InnoDB');
    }

    public function down()
    {
        $this->dropTable('users');
    }
}
