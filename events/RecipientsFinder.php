<?php
/**
 *
 * User: psi
 * Date: 23.01.17
 * Time: 14:29
 */

namespace app\events;


use app\models\EventNotification;
use app\models\EventNotificationRecipient;
use app\models\User;
use yii\base\InvalidParamException;

class RecipientsFinder
{
    /**
     * @var int
     */
    public $batchSize = 100;

    /**
     * Returns users list to notify
     * @param EventNotification $notification
     * @param mixed $sender
     * @return User[]
     */
    public function find(EventNotification $notification, $sender)
    {
        $query = User::find();

        if ($notification->recipient_type === EventNotification::RECIPIENTS_ALL) {

            // do nothing ...

        } elseif ($notification->recipient_type === EventNotification::RECIPIENTS_ROLE) {

            $userIds = \Yii::$app->authManager->getUserIdsByRole($notification->recipient_value);
            $query->andWhere(['id' => $userIds]);

        } elseif ($notification->recipient_type === EventNotification::RECIPIENTS_OWNER) {

            if ($sender instanceof NotifiableInterface) {
                $query->andWhere(['id' => $sender->getOwnerId()]);
            } else {
                throw new InvalidParamException("Sender must implements " . NotifiableInterface::class);
            }

        } elseif ($notification->recipient_type === EventNotification::RECIPIENTS_GROUP) {

            $query->innerJoin(EventNotificationRecipient::tableName() . ' enr', 'enr.user_id = users.id');
            $query->andWhere('enr.notification_id = :notification_id', [':notification_id' => $notification->id]);

        }

        return $query->each($this->batchSize);
    }
}