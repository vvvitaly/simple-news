<?php
/**
 *
 * User: psi
 * Date: 22.01.17
 * Time: 21:53
 */

namespace app\events;


use app\models\User;

interface TransportInterface
{
    /**
     * Check if transport can send a message to user
     * @param User $recipient
     * @return boolean
     */
    public function canSend(User $recipient);

    /**
     * Send a message to user
     * @param User $recipient
     * @param MessageInterface $message
     * @return bool
     */
    public function send(User $recipient, MessageInterface $message);

    /**
     * Class name for message model implements TransportMessageInterface
     * @return string
     */
    public function getMessageModelClass();

    /**
     * Message view
     * @return string
     */
    public function getMessageFormView();

    /**
     * Serialize message model to save in [[EventNotification::$content_template]]
     * @param MessageInterface $model
     * @return string
     */
    public function serializeMessageModel($model);

    /**
     * Unserialize contents to message model attributes
     * @param $contents
     * @return MessageInterface
     */
    public function unserializeMessageModel($contents);
}