<?php
/**
 *
 * User: psi
 * Date: 25.01.17
 * Time: 0:43
 */

namespace app\events;


interface MessageInterface
{
    /**
     * @param Renderer $renderer
     * @param mixed $sender
     * @return MessageInterface
     */
    public function render(Renderer $renderer, $sender);
}