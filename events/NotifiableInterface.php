<?php
/**
 *
 * User: psi
 * Date: 23.01.17
 * Time: 22:42
 */

namespace app\events;


interface NotifiableInterface
{
    public function getOwnerId();
}