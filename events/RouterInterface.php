<?php
/**
 *
 * User: psi
 * Date: 23.01.17
 * Time: 9:25
 */

namespace app\events;


use app\models\EventNotification;
use yii\base\Event;

interface RouterInterface
{
    /**
     * Find notifications objects by event
     *
     * @param Event $event
     * @return EventNotification[]
     */
    public function findNotifications(Event $event);
}