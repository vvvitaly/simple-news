<?php
/**
 *
 * User: psi
 * Date: 22.01.17
 * Time: 21:45
 */

namespace app\events;


use app\models\EventNotification;
use UrbanIndo\Yii2\Queue\Job;
use UrbanIndo\Yii2\Queue\Queue;
use yii\base\Component;
use yii\base\Event;
use yii\di\Instance;

class Notifier extends Component
{
    /**
     * @var array List of listened events. Each item is array of two elements: target (object, class, interface) and
     * event name.
     */
    public $events = [];

    /**
     * @var RouterInterface
     */
    public $router = [
        'class' => DbRouter::class,
    ];

    /**
     * @var Queue
     * TODO: make interface
     */
    public $queue = 'queue';

    public $queueRouteSend = 'notifications/send';

    /**
     * @var EventSerializer
     */
    public $serializer = [
        'class' => EventSerializer::class,
    ];

    /**
     * @var TransportInterface[]
     */
    public $transports = [];

    /**
     * @var Renderer
     */
    public $renderer = [
        'class' => Renderer::class,
    ];

    /**
     * @var RecipientsFinder
     */
    public $recipientsFinder = [
        'class' => RecipientsFinder::class
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->router = Instance::ensure($this->router, RouterInterface::class);
        $this->queue = Instance::ensure($this->queue, Queue::class);
        $this->serializer = Instance::ensure($this->serializer, EventSerializer::class);
        $this->renderer = Instance::ensure($this->renderer, Renderer::class);
        $this->recipientsFinder = Instance::ensure($this->recipientsFinder, RecipientsFinder::class);
        foreach ($this->transports as &$transport) {
            $transport = Instance::ensure($transport, TransportInterface::class);
        }
        unset($transport);
    }

    /**
     * Enable events listening
     */
    public function listen()
    {
        $handler = [&$this, 'routeEvent'];
        foreach ($this->events as list($target, $event)) {
            if (is_string($target)) {
                Event::on($target, $event, $handler);
            } else {
                throw new \InvalidArgumentException("Target must be component class name");
            }
        }
    }

    /**
     * Find notifications & send for the event
     * @param Event $event
     * @return bool
     */
    public function routeEvent(Event $event)
    {
        $notifications = $this->router->findNotifications($event);
        foreach ($notifications as $notification) {
            $this->applyNotification($notification, $event);
        }

        return count($notifications) > 0;
    }

    /**
     * Send specified notification task to queue
     * @param EventNotification $notification
     * @param Event $event caused event
     * @return boolean
     */
    public function applyNotification(EventNotification $notification, Event $event)
    {
        $jobArgs = [
            'id' => $notification->id,
            'event' => $event->name,
            'sender' => $this->serializer->serializeSender($event->sender),
        ];
        return $this->queue->post(new Job(['route' => $this->queueRouteSend, 'data' => $jobArgs]));
    }

    /**
     * Send notification
     * @param int $notificationId
     * @param string $event
     * @param mixed $eventSender
     * @throws \InvalidArgumentException
     */
    public function send($notificationId, $event, $eventSender)
    {
        $notification = $this->findNotification($notificationId);
        if ($notification === null) {
            throw new \InvalidArgumentException("Can not find notification #$notificationId");
        }

        $transport = $this->getTransport($notification->transport_name);
        if ($transport === null) {
            throw new \InvalidArgumentException("Can not find transport '{$notification->transport_name}'");
        }

        $message = $transport->unserializeMessageModel($notification->content_template);
        $sender = null;
        if ($eventSender) {
            $sender = $this->serializer->unserializeSender($eventSender);
            $message = $message->render($this->renderer, $sender);
        }

        foreach ($this->recipientsFinder->find($notification, $sender) as $user) {
            if ($transport->canSend($user)) {
                $transport->send($user, $message);
            }
        }

        $notification->autoremove();
    }

    /**
     * Find notification model by ID
     * @param int $id
     * @return EventNotification|null
     */
    protected function findNotification($id)
    {
        return EventNotification::findOne($id);
    }

    /**
     * @param $name
     * @return TransportInterface|null
     */
    protected function getTransport($name)
    {
        return isset($this->transports[$name])? $this->transports[$name]: null;
    }
}