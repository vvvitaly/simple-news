<?php
/**
 *
 * User: psi
 * Date: 23.01.17
 * Time: 9:45
 */

namespace app\events;


use yii\base\Object;
use yii\helpers\ArrayHelper;

class Renderer extends Object
{
    /**
     * Render notification content template
     * @param string $template content template
     * @param mixed $eventSender event initiator (@see [[\yii\base\Event::$sender]])
     * @return string
     */
    public function render($template, $eventSender)
    {
        preg_match_all('/\{\{(.+?)\}\}/', $template, $matches);
        $vars = $this->buildReplacement($eventSender, $matches[1]);

        return strtr($template, $vars);
    }

    protected function buildReplacement($sender, $vars)
    {
        $root = new \stdClass();
        $root->sender = $sender;

        $data = [];
        foreach ($vars as $var) {
            $data[$this->makeReplacementVar($var)] = ArrayHelper::getValue($root, $var, '');
        }

        return $data;
    }

    protected function makeReplacementVar($key)
    {
        return '{{' . $key . '}}';
    }
}