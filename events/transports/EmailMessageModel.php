<?php
/**
 *
 * User: psi
 * Date: 24.01.17
 * Time: 17:19
 */

namespace app\events\transports;


use app\events\MessageInterface;
use app\events\Renderer;
use yii\base\Model;

class EmailMessageModel extends Model implements MessageInterface
{
    public $subject;
    public $body;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'subject' => 'Email Subject',
            'body' => 'Email body'
        ];
    }

    public function rules()
    {
        return [
            [['subject', 'body'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function render(Renderer $renderer, $sender)
    {
        return new static([
            'subject' => $renderer->render($this->subject, $sender),
            'body' => $renderer->render($this->body, $sender),
        ]);
    }
}