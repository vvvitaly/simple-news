<?php
/**
 *
 * User: psi
 * Date: 24.01.17
 * Time: 16:05
 */

namespace app\events\transports;


use app\events\MessageInterface;
use app\events\TransportInterface;
use app\models\User;
use yii\base\Object;
use yii\di\Instance;
use yii\mail\MailerInterface;

class Email extends Object implements TransportInterface
{
    /**
     * @var MailerInterface
     */
    public $mailer = 'mailer';

    /**
     * @inheritdoc
     */
    public function canSend(User $recipient)
    {
        return (boolean)$recipient->is_allow_emails;
    }

    /**
     * @inheritdoc
     * @param EmailMessageModel $message
     */
    public function send(User $recipient, MessageInterface $message)
    {
        $this->mailer = Instance::ensure($this->mailer, MailerInterface::class);
        if ($this->canSend($recipient)) {
            $this->mailer->compose()
                ->setFrom([\Yii::$app->params['emailFrom'] => \Yii::$app->params['brand']])
                ->setTo([$recipient->email => $recipient->name])
                ->setSubject($message->subject)
                ->setHtmlBody($message->body)
                ->send();
        }
        return false;
    }

    public function getMessageModelClass()
    {
        return EmailMessageModel::class;
    }

    public function getMessageFormView()
    {
        return '@app/views/_transports/email.php';
    }

    /**
     * @param EmailMessageModel $model
     * @return string
     */
    public function serializeMessageModel($model)
    {
        return json_encode($model->toArray());
    }

    /**
     * @param string $contents
     * @return EmailMessageModel
     */
    public function unserializeMessageModel($contents)
    {
        return new EmailMessageModel(json_decode($contents, false));
    }
}