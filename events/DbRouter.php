<?php
/**
 *
 * User: psi
 * Date: 22.01.17
 * Time: 22:40
 */

namespace app\events;


use app\models\EventNotification;
use yii\base\Event;
use yii\base\Object;

class DbRouter extends Object implements RouterInterface
{
    const FILTER_MASK = '*';

    /**
     * @var string
     */
    public $modelClass = EventNotification::class;

    /**
     * @inheritdoc
     */
    public function findNotifications(Event $e)
    {
        $class = $this->modelClass;
        return $class::find()->where(
            [
                'and',
                ['or', 'target_class=:all', 'target_class=:className'],
                ['or', 'target_event=:all', 'target_event=:eventName'],
            ],
            [
                ':all' => self::FILTER_MASK,
                ':className' => get_class($e->sender),
                ':eventName' => $e->name
            ]
        )->all();
    }
}