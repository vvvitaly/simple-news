<?php
/**
 *
 * User: psi
 * Date: 23.01.17
 * Time: 14:29
 */

namespace app\events;


use yii\base\Object;
use yii\db\BaseActiveRecord;
use yii\helpers\StringHelper;

class EventSerializer extends Object
{
    const SIGN_ACTIVE_RECORD = ':ar:';
    const SIGN_NULL = ':null:';

    /**
     * Serialize event sender to pass it to queue job
     * @param mixed $eventSender
     * @return mixed
     */
    public function serializeSender($eventSender)
    {
        if ($eventSender === null) {
            return [self::SIGN_NULL];
        } elseif ($eventSender instanceof BaseActiveRecord) {
            return [self::SIGN_ACTIVE_RECORD, get_class($eventSender), $eventSender->primaryKey];
        }
        return $eventSender;
    }

    /**
     * Unserialize sender when it passed to job
     * @param mixed $serializedEventSender
     * @return mixed
     */
    public function unserializeSender($serializedEventSender)
    {
        $special = is_array($serializedEventSender)
            && isset($serializedEventSender[0])
            && StringHelper::startsWith($serializedEventSender[0], ':') ? $serializedEventSender[0]: null;

        if ($special) {
            if ($special === self::SIGN_ACTIVE_RECORD) {
                $class = $serializedEventSender[1];
                $pk = $serializedEventSender[2];
                return $class::findOne($pk);
            } elseif ($special === self::SIGN_NULL) {
                return null;
            }
        }
        return $serializedEventSender;
    }
}