<?php
/**
 *
 * User: psi
 * Date: 19.01.17
 * Time: 22:05
 */
use app\models\User;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var User $model */

$this->title = 'Profile';
$this->params['breadcrumbs'][] = 'Profile';
?>

<div class="profile-index">

    <?php
        echo Html::beginForm(['reset-password'], 'post', ['id' => 'reset-password-form']);
        echo Html::submitButton('Reset Password', ['class' => 'btn btn-warning']);
        echo Html::endForm();
    ?>

    <hr>

    <?php $form = ActiveForm::begin([]); ?>

    <?= $form->field($model, 'is_allow_emails')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
