<?php
/**
 *
 * User: psi
 * Date: 20.01.17
 * Time: 9:28
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\SignupForm */
/* @var $allowForm boolean */

$this->title = 'Change Password';
$this->params['breadcrumbs'][] = 'Change Password';
?>

<div class="profile-change-password">

    <?php if ($allowForm): ?>

        <?php $form = ActiveForm::begin([
            'id' => 'change-password-form',
            'layout' => 'horizontal',
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-7\">{error}</div>",
                'labelOptions' => ['class' => 'col-lg-2 control-label'],
            ],
        ]); ?>

        <?= $form->field($model, 'password')->passwordInput(['autofocus' => true]) ?>
        <?= $form->field($model, 'password_repeat')->passwordInput() ?>

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton('Change', ['class' => 'btn btn-primary', 'name' => 'change-password-button']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    <?php else: ?>

        <?= Html::errorSummary($model, ['class' => 'alert alert-danger', 'header' => '']) ?>

    <?php endif; ?>
</div>
