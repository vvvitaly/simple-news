<?php

use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Inflector;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Notifications';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-notification-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Notification', ['create'], ['class' => 'btn btn-success btn-sm']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'target_class',
            [
                'attribute' => 'target_event',
                'value' => function($model, $key, $index, $column) {
                    return Inflector::titleize($model->target_event);
                }
            ],
            [
                'attribute' => 'transport_name',
                'value' => function($model, $key, $index, $column) {
                    return Inflector::titleize($model->transport_name);
                }
            ],
            'recipient',

            [
                'class' => ActionColumn::class,
                'template' => '{view} {update} {delete} {send}',
                'buttons' => [
                    'send' => function ($url, $model, $key) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-envelope"></span>',
                            ['manual', 'id' => $model->id]);
                    }
                ],
            ],
        ],
    ]); ?>
</div>
