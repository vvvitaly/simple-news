<?php
/**
 *
 * User: psi
 * Date: 24.01.17
 * Time: 20:41
 */
use app\assets\EventsFormAsset;
use app\controllers\EventsController;
use app\models\EventNotification;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\bootstrap\Tabs;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\EventNotification */
/* @var $originalId integer */
/* @var $usersProvider ActiveDataProvider */

/** @var EventsController $context */
$context = $this->context;
$roles = ArrayHelper::map($context->getRoles(), 'name', 'name');

$this->title = 'Send Event Notification #' . $originalId;
$this->params['breadcrumbs'][] = ['label' => 'Event Notifications', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Notification #' . $originalId, 'url' => ['view', 'id' => $originalId]];
$this->params['breadcrumbs'][] = 'Send';

$options = Json::encode([
    'recipientTypeInput' => '#' . Html::getInputId($model, 'recipient_type'),
    'addRecipientUrl' => Url::to(['add-user', 'id' => $model->id, 'user' => '__ID__', 'remove' => '__REMOVE__']),
]);
$this->registerJs("$('#event-send-form').sendForm({$options});", View::POS_READY, 'events-send-form');

EventsFormAsset::register($this);
?>

<div class="event-notification-send-form">

    <?php $form = ActiveForm::begin(['options' => ['id' => 'event-send-form']]); ?>

    <?= $form->field($model, 'displayedTarget')->staticControl() ?>
    <?= $form->field($model, 'displayedEvent')->staticControl() ?>
    <?= $form->field($model, 'displayedTransport')->staticControl() ?>

    <?= $form->field($model, 'recipient_type')->hiddenInput() ?>

    <div class="form-group">
        <?= Tabs::widget([
            'navType' => 'nav-pills',
            'items' => [
                [
                    'label' => 'All',
                    'headerOptions' => ['data' => [
                        'recipient-type' => EventNotification::RECIPIENTS_ALL,
                        'next' => 'send',
                    ]],
                    'active' => $model->recipient_type === EventNotification::RECIPIENTS_ALL,
                ],
                [
                    'label' => 'Role',
                    'content' => $form->field($model, 'recipient_value')->dropDownList($roles, ['prompt' => '']),
                    'headerOptions' => ['data' => [
                        'recipient-type' => EventNotification::RECIPIENTS_ROLE,
                        'next' => 'send',
                    ]],
                    'active' => $model->recipient_type === EventNotification::RECIPIENTS_ROLE,
                ],
                [
                    'label' => 'Group Users',
                    'headerOptions' => ['data' => [
                        'recipient-type' => EventNotification::RECIPIENTS_GROUP,
                        'next' => 'users',
                    ]],
                    'active' => $model->recipient_type === EventNotification::RECIPIENTS_GROUP,
                    'content' => '',
                ],
            ],
        ]) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Next', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>