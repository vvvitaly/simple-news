<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EventNotification */

$this->title = 'Update Event Notification: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Event Notifications', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Notification #' . $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="event-notification-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Edit Message', ['edit-message', 'id' => $model->id], ['class' => 'btn btn-success btn-sm']) ?>
    </p>

    <?= $this->render('_form', ['model' => $model]) ?>

</div>
