<?php
/**
 *
 * User: psi
 * Date: 24.01.17
 * Time: 22:42
 */

use app\assets\EventsFormAsset;
use app\models\User;
use yii\web\View;
use yii\bootstrap\Html;
use yii\data\ActiveDataProvider;
use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\Json;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $notification app\models\EventNotification */
/* @var $originalId integer */
/* @var $dataProvider ActiveDataProvider */

$this->title = 'Add Users';
$this->params['breadcrumbs'][] = $this->title;

$options = Json::encode([
    'recipientTypeInput' => '#' . Html::getInputId($notification, 'recipient_type'),
    'addRecipientUrl' => Url::to(['add-user', 'id' => $notification->id, 'user' => '__ID__', 'remove' => '__REMOVE__']),
]);
$this->registerJs("$('#users-grid').usersGrid({$options});", View::POS_READY, 'events-add-users');

EventsFormAsset::register($this);
?>

<div class="notifications-add-users">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'options' => ['id' => 'users-grid', 'class' => 'grid-view'],
        'dataProvider' => $dataProvider,
        'rowOptions' => function($model, $key, $index, $grid) use($notification) {
            $selected = isset($model->notifications[$notification->id]);
            $options = [];
            if ($selected) {
                $options['class'] = 'info';
            }
            return $options;
        },
        'columns' => [
            [
                'class' => CheckboxColumn::class,
                'checkboxOptions' => function($model, $key, $index, $column) use ($notification) {
                    /** @var User $model */
                    return [
                        'checked' => isset($model->notifications[$notification->id])
                    ];
                },
                'cssClass' => 'event-recipient',
                'contentOptions' => ['class' => 'text-center']
            ],
            'email:email',
            'name',
        ],
    ]); ?>

    <p>
        <a href="<?= Url::to(['send', 'id' => $notification->id]) ?>" class="btn btn-success">Continue</a>
    </p>
</div>

