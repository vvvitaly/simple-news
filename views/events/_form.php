<?php

use app\assets\EventsFormAsset;
use app\controllers\EventsController;
use app\models\EventNotification;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EventNotification */
/* @var $form yii\widgets\ActiveForm */

/** @var EventsController $context */
$context = $this->context;
$roles = ArrayHelper::map($context->getRoles(), 'name', 'name');

$options = Json::encode([
    'recipientSelectId' => Html::getInputId($model, 'recipient_type'),
    'recipientsWithRoles' => [EventNotification::RECIPIENTS_ROLE],
    'roleSelectId' => Html::getInputId($model, 'recipient_value'),
    'targetSelectId' => Html::getInputId($model, 'target_class'),
    'eventSelectId' => Html::getInputId($model, 'target_event'),
    'events' => $context->getEvents(),
]);
$this->registerJs("$('#events-form').notificationForm({$options});", View::POS_READY, 'events-edit-form');

EventsFormAsset::register($this);
?>

<div class="event-notification-form">

    <?php $form = ActiveForm::begin(['options' => ['id' => 'events-form']]); ?>

    <?= $form->field($model, 'target_class')->dropDownList($context->getInitiators(), ['prompt' => '']) ?>
    <?= $form->field($model, 'target_event')->dropDownList([[]]) ?>
    <?= $form->field($model, 'recipient_type')->dropDownList([
        'all' => 'All', 'role' => 'Role', 'owner' => 'Owner'], ['prompt' => '']) ?>
    <?= $form->field($model, 'recipient_value')->dropDownList($roles, ['prompt' => '']) ?>
    <?= $form->field($model, 'transport_name')->dropDownList($context->getTransports(), ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>