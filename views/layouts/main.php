<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\auth\Permissions;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title . ' | ' . Yii::$app->params['brand']) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<?php
NavBar::begin([
    'brandLabel' => Yii::$app->params['brand'],
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar',
    ],
    'containerOptions' => ['id' => 'top-menu'],
]);
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items' => [
        ['label' => 'News', 'url' => ['/news/index']],
        ['label' => 'Users', 'url' => ['/users/index'], 'visible' => Yii::$app->user->can(Permissions::VIEW_USERS)],
        ['label' => 'Events', 'url' => ['/events/index'], 'visible' => Yii::$app->user->can(Permissions::VIEW_NOTIFICATIONS)],
        ['label' => 'Profile', 'url' => ['/profile/index'], 'visible' => !Yii::$app->user->isGuest],
        Yii::$app->user->isGuest ? (
            ['label' => 'Login', 'url' => ['/site/login']]
        ) : (
            '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->shortName . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>'
        )
    ],
]);
NavBar::end();
?>

<div class="container">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>

    <?php if (empty($this->params['skipAlerts'])): ?>

        <?php if (!Yii::$app->user->isGuest && !Yii::$app->user->identity->is_activated): ?>

         <div class="alert alert-warning" role="alert">
             Activation email was sent.
             Please follow the instructions about how to activate your account.
             <a href="#" class="alert-link">Resend email</a>
         </div>

        <?php endif; ?>

        <?php if (Yii::$app->session->hasFlash('alert-success')): ?>

            <?php foreach (Yii::$app->session->getFlash('alert-success') as $flash): ?>
                <div class="alert alert-success" role="alert">
                    <?= $flash ?>
                </div>
            <?php endforeach; ?>

        <?php endif; ?>

    <?php endif; ?>

    <?= $content ?>
</div>

<?php $this->endBody() ?>

</body>
</html>

<?php $this->endPage() ?>
