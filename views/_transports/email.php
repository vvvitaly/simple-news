<?php
/**
 *
 * User: psi
 * Date: 24.01.17
 * Time: 17:20
 */

use app\events\transports\EmailMessageModel;
use app\models\EventNotification;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model EmailMessageModel */
/* @var $notification EventNotification */

$this->title = 'Edit Notification Message';
$this->params['breadcrumbs'][] = ['label' => 'Event Notifications', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Notification #' . $notification->id, 'url' => ['view', 'id' => $notification->id]];
$this->params['breadcrumbs'][] = 'Edit Message';
?>
<div class="event-notification-message-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'subject')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'body')->textarea(['rows' => 12]) ?>


    <div class="form-group">
        <?= Html::submitButton('Update', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>