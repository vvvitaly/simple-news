<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
$this->params['skipAlerts'] = true;
?>
<div class="site-error">
    <h1>Ooups!</h1>
    <div class="alert alert-danger"><?= Html::encode($this->title) ?></div>

    <p><?= nl2br(Html::encode($message)) ?></p>
</div>
