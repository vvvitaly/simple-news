<?php
/**
 *
 * User: psi
 * Date: 18.01.17
 * Time: 23:28
 */
use app\models\AccountActivationForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form AccountActivationForm */

$this->title = 'Account Activation';
?>

<div class="site-activation">

    <?php if ($form->errors): ?>
        <?= Html::errorSummary($form, ['class' => 'alert alert-danger', 'header' => '']) ?>
    <?php endif; ?>

</div>