<?php
/**
 *
 * User: psi
 * Date: 20.01.17
 * Time: 23:07
 */
use app\models\AccountActivationForm;

/* @var $this yii\web\View */
/* @var $activation AccountActivationForm */

?>

Hi, <?= $activation->getActivationModel()->user->name ?><br />
Your account was activated successfully!
