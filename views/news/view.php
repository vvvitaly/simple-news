<?php

use app\auth\Permissions;
use yii\bootstrap\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\News */

$user = Yii::$app->user;

$this->title = $model->title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view">

    <div class="row">
        <div class="col-lg-8">
        <h1><?= Html::encode($this->title) ?></h1>
        </div>
        <div class="col-lg-4 text-right">
            <?php if ($user->can(Permissions::EDIT_NEWS_CONTENTS)): ?>
            <?= Html::a(Html::icon('edit') . ' Edit', ['edit', 'id' => $model->id], [
                    'class' => 'btn btn-primary btn-xs btn-news-edit'
            ]) ?>
            <?php endif; ?>

            <?php if ($user->can(Permissions::EDIT_NEWS_CONTENTS)): ?>
            <?= Html::a(Html::icon('remove') . ' Remove', ['remove', 'id' => $model->id], [
                'class' => 'btn btn-danger btn-xs btn-news-remove',
                'data' => [
                    'confirm' => 'Sure?',
                    'method' => 'post',
                ],
            ]) ?>
            <?php endif; ?>
        </div>
    </div>

    <div class="row news-body">
        <div class="col-lg-12">
            <p>
                <?= $model->user->name ?> / <em><?= Yii::$app->formatter->asDate($model->created_at) ?></em>
            </p>
            <p class="news-content lead"><?= Html::encode($model->body) ?></p>
        </div>
    </div>

</div>
