<?php


use app\auth\Permissions;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$user = Yii::$app->user;

$this->title = 'News';
?>
<div class="news-index">
    <div class="body-content news-list">
        <?php if ($user->can(Permissions::CREATE_NEWS)): ?>
        <p>
            <?= Html::a('Create Article', ['create'], ['class' => 'btn btn-success btn-sm']) ?>
        </p>
        <?php endif; ?>

        <div class="row">
            <?php $idx = 1; foreach ($dataProvider->getModels() as $newsModel): ?>

                <div class="col-lg-4">
                    <h3><?= Html::encode($newsModel->title) ?></h3>

                    <p class="news-content"><?= Html::encode($newsModel->preview) ?></p>

                    <p class="news-control">
                        <?php if ($user->can(Permissions::VIEW_NEWS_CONTENTS)): ?>
                        <a class="btn btn-link btn-news-view" href="<?= Url::to(['view', 'id' => $newsModel->id]) ?>">More &raquo;</a>
                        <?php endif; ?>

                        <?php if ($user->can(Permissions::EDIT_NEWS_CONTENTS)): ?>
                        <a class="btn btn-link btn-news-edit" href="<?= Url::to(['edit', 'id' => $newsModel->id]) ?>">
                            <?= Html::icon('edit') ?> Edit
                        </a>
                        <?php endif; ?>

                        <?php if ($user->can(Permissions::REMOVE_NEWS)): ?>
                        <?= Html::a(
                                Html::icon('remove') . ' Remove',
                                ['remove', 'id' => $newsModel->id],
                                [
                                    'class' => 'btn btn-link text-danger btn-news-remove',
                                    'data' => [
                                        'confirm' => 'Sure?',
                                        'method' => 'post',
                                        'pjax' => 0,
                                    ],
                                ]
                        )?>
                        <?php endif; ?>
                    </p>
                </div>

                <?= $idx % 3 === 0? Html::tag('div', '', ['class' => 'clearfix visible-lg-block']): '' ?>

            <?php $idx++; endforeach; ?>
        </div>

        <div class="row">
            <div class="col-lg-12 text-right">
                <?= LinkPager::widget([
                    'options' => ['class' => 'pagination pagination-sm'],
                    'pagination' => $dataProvider->pagination,
                ]) ?>
            </div>
        </div>
    </div>
</div>
