<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Update User: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

    <hr>

    <?php
        echo Html::beginForm(['reset-password', 'id' => $model->id], 'post', ['id' => 'reset-password-form', 'method' => 'post']);
        echo Html::submitButton('Reset Password', ['class' => 'btn btn-danger']);
        echo Html::endForm();
    ?>

</div>
