<?php

use app\models\User;
use app\models\UserActivationCode;
use app\tests\fixtures\AuthAssignmentFixture;
use app\tests\fixtures\NewsFixture;
use app\tests\fixtures\RealEventNotificationsFixture;
use app\tests\fixtures\UserFixture;
use yii\mail\MessageInterface;

class SignupFormCest
{
    public function _fixtures()
    {
        return [
            'news' => NewsFixture::class,
            'users' => UserFixture::class,
            AuthAssignmentFixture::class,
            RealEventNotificationsFixture::class,
        ];
    }

    public function _before(\FunctionalTester $I)
    {
        $I->amOnRoute('site/signup');
    }

    public function openSignupPage(\FunctionalTester $I)
    {
        $I->seeInTitle('Sign Up');
    }

    public function signupWithEmptyCredentials(FunctionalTester $I)
    {
        $I->submitForm('#signup-form', []);
        $I->expectTo('see validations errors');
        $I->see('Email cannot be blank.');
        $I->see('Password cannot be blank.');
    }

    public function signupWithWrongCredentials(\FunctionalTester $I)
    {
        $I->submitForm('#signup-form', [
            'SignupForm[email]' => 'some-wrong-email',
        ]);
        $I->expectTo('see validations errors');
        $I->see('Email is not a valid email address.');
        $I->see('Password cannot be blank.');
        $I->see('Repeat Password cannot be blank.');

        $I->submitForm('#signup-form', [
            'SignupForm[email]' => uniqid('test-', true) . '@example.com',
            'SignupForm[password]' => '123456',
            'SignupForm[password_repeat]' => '654321',
        ]);
        $I->expectTo('see validations errors');
        $I->see('Password must be equal to "Repeat Password".');
    }

    public function signupWithExistingEmail(\FunctionalTester $I)
    {
        $I->haveFixtures(['users' => UserFixture::class]);
        $user = $I->grabFixture('users', 0);

        $I->submitForm('#signup-form', [
            'SignupForm[email]' => $user->email,
            'SignupForm[password]' => '123456',
            'SignupForm[password_repeat]' => '123456',
        ]);
        $I->expectTo('see validations errors');
        $I->see('Email "' . $user->email . '" has already been taken.');
    }

    public function signupSuccessfully(\FunctionalTester $I)
    {
        $rand = 'test-' . time();
        $email = $rand . '@example.com';
        $name = $rand . '-name';
        $I->submitForm('#signup-form', [
            'SignupForm[email]' => $email,
            'SignupForm[password]' => '123456',
            'SignupForm[password_repeat]' => '123456',
            'SignupForm[name]' => $name,
        ]);

        $user = $I->grabRecord(User::class, ['email' => $email]);
        $uac = $I->grabRecord(
            UserActivationCode::class,
            ['user_id' => $user->id, 'type' => UserActivationCode::TYPE_ACTIVATE_ACCOUNT, 'is_used' => 0]
        );

        $I->see('Logout (test-');
        $I->dontSeeElement('form#signup-form');
        $I->see('Please follow the instructions about how to activate your account.', '.alert-warning');
        /** @var MessageInterface $email */
        $message = $I->grabLastSentEmail();
        $I->assertEquals([$email => $name], $message->getTo());
        $I->assertEquals('Activate your account', $message->getSubject());
    }
}