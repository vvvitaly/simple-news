<?php
/**
 *
 * User: psi
 * Date: 18.01.17
 * Time: 22:37
 */

namespace app\tests\functional;


use app\models\User;
use app\models\UserActivationCode;
use app\tests\fixtures\UserFixture;

class ActivateAccountCest
{
    const NOT_ACTIVATED_USER = 103;

    public function _fixtures()
    {
        return ['users' => UserFixture::class];
    }

    public function activateAccountWithWrongCode(\FunctionalTester $I)
    {
        $I->amOnRoute('site/activation', ['code' => '000']);
        $I->see('Wrong activation code.');
    }

    public function activateAccountWithExpiredCode(\FunctionalTester $I)
    {
        $code = uniqid('activate-');
        $I->haveRecord(UserActivationCode::class, [
            'created_at' => '2017-01-01 00:00:00',
            'active_to' => '2017-01-01 00:00:00',
            'user_id' => self::NOT_ACTIVATED_USER,
            'type' => UserActivationCode::TYPE_ACTIVATE_ACCOUNT,
            'code' => $code,
            'is_used' => 0,
        ]);

        $I->amOnRoute('site/activation', ['code' => $code]);
        $I->see('Wrong activation code.');
    }

    public function activateAccountWithUsedCode(\FunctionalTester $I)
    {
        $code = uniqid('activate-');
        $I->haveRecord(UserActivationCode::class, [
            'created_at' => '2017-01-01 00:00:00',
            'active_to' => '2999-01-01 00:00:00',
            'user_id' => self::NOT_ACTIVATED_USER,
            'type' => UserActivationCode::TYPE_ACTIVATE_ACCOUNT,
            'code' => $code,
            'is_used' => 1,
        ]);

        $I->amOnRoute('site/activation', ['code' => $code]);
        $I->see('Wrong activation code.');
    }

    public function activateAccountWithCorrectCode(\FunctionalTester $I)
    {
        $code = uniqid('activate-');
        $user = $I->grabRecord(User::class, ['id' => self::NOT_ACTIVATED_USER]);

        $I->haveRecord(UserActivationCode::class, [
            'created_at' => '2017-01-01 00:00:00',
            'active_to' => '2999-01-01 00:00:00',
            'user_id' => self::NOT_ACTIVATED_USER,
            'type' => UserActivationCode::TYPE_ACTIVATE_ACCOUNT,
            'code' => $code,
            'is_used' => 0,
        ]);

        $I->amOnRoute('site/activation', ['code' => $code]);
        $I->see('Hi, ' . $user->name);
        $I->see('Your account was activated successfully!');
        $I->see('Logout (');
        $I->dontSee('Please follow the instructions about how to activate your account.');
    }

    public function activateAccountWithCorrectCodeWhenLoggedIn(\FunctionalTester $I)
    {
        $code = uniqid('activate-');
        $user = $I->grabRecord(User::class, ['id' => self::NOT_ACTIVATED_USER]);
        $I->amLoggedInAs(self::NOT_ACTIVATED_USER);

        $I->haveRecord(UserActivationCode::class, [
            'created_at' => '2017-01-01 00:00:00',
            'active_to' => '2999-12-31 11:59:59',
            'user_id' => self::NOT_ACTIVATED_USER,
            'type' => UserActivationCode::TYPE_ACTIVATE_ACCOUNT,
            'code' => $code,
            'is_used' => 0,
        ]);

        $I->amOnRoute('site/activation', ['code' => $code]);
        $I->see('Hi, ' . $user->name);
        $I->see('Your account was activated successfully!');
        $I->dontSee('Please follow the instructions about how to activate your account.');
    }
}