<?php
/**
 *
 * User: psi
 * Date: 22.01.17
 * Time: 17:07
 */

namespace app\tests\functional;


use app\tests\fixtures\AuthAssignmentFixture;
use app\tests\fixtures\EmptyEventNotificationFixture;
use app\tests\fixtures\NewsFixture;
use app\tests\fixtures\RealEventNotificationsFixture;
use app\tests\fixtures\UserFixture;
use pages\NewsPage;
use pages\UsersPage;

class RbacCest
{
    const ADMIN = 100;
    const MODERATOR = 101;
    const USER = 102;

    public static $PAGES = [
        [['site/login'], ['guest', 'user', 'moderator', 'admin']],
        [['site/signup'], ['guest', 'user', 'moderator', 'admin']],
        [['site/activation', 'code' => '0000'], ['guest', 'user', 'moderator', 'admin']],
        [['profile/index'], ['user', 'moderator', 'admin']],
        [['profile/change-password', 'code' => '0000'], ['guest', 'user', 'moderator', 'admin']],
        [['news/index'], ['guest', 'user', 'moderator', 'admin']],
        [['news/view'], ['user', 'moderator', 'admin']],
        [['news/edit', 'id' => 100], ['moderator', 'admin']],
        [['news/create'], ['moderator', 'admin']],
        [['news/remove', 'id' => 100], ['moderator', 'admin']],
        [['users/index'], ['admin']],
        [['users/create'], ['admin']],
        [['users/update', 'id' => 105], ['admin']],
        [['users/delete', 'id' => 105], ['admin']],
        [['users/reset-password', 'id' => 105], ['admin']],
        [['events/index'], ['admin']],
        [['events/create'], ['admin']],
        [['events/update', 'id' => 104], ['admin']],
        [['events/delete', 'id' => 104], ['admin']],
        [['events/manual', 'id' => 104], ['admin']],
        [['events/add-users', 'id' => 104], ['admin']],
        [['events/send', 'id' => 104], ['admin']],
        [['events/edit-message', 'id' => 104], ['admin']],
    ];

    public static $ROUTES_ALLOWED_FOR_GUESTS = [
        'site/activation',
        'profile/change-password',
        'news/index',
    ];

    public function _fixtures()
    {
        return [
            'users' => UserFixture::class,
            'news' => NewsFixture::class,
            EmptyEventNotificationFixture::class,
            AuthAssignmentFixture::class
        ];
    }

    public function _visitRoutes(\FunctionalTester $I, $role)
    {
        $I->am($role);
        $I->expectTo('visit some denied pages');
        foreach(self::$PAGES as list($page, $allowedRoles)) {
            $I->amOnPage($page);
            if (in_array($role, $allowedRoles)) {
                $I->dontSee('Forbidden (#403)');
            } else {
                if ($role === 'guest') {
                    $I->seeElement('#login-form');
                } else {
                    $I->see('Forbidden (#403)');
                }
            }
        }
    }

    public function asGuest(\FunctionalTester $I)
    {
        $this->_visitRoutes($I, 'guest');

        // I see only views previews
        $I->amOnRoute(NewsPage::LIST_URL);
        $I->see('News', NewsPage::MENU_ELEMENT);
        $I->see('Login', NewsPage::MENU_ELEMENT);
        $I->dontSee('Users', NewsPage::MENU_ELEMENT);
        $I->dontSee('Profile', NewsPage::MENU_ELEMENT);
        $I->dontSee('Create Article', '.btn');
        $I->dontSee('More', '.btn');
        $I->dontSee('Edit', '.btn');
        $I->dontSee('Remove', '.btn');
    }

    public function asUser(\FunctionalTester $I)
    {
        $I->amLoggedInAs(self::USER);
        $this->_visitRoutes($I, 'user');

        // I see views previews ...
        $I->amOnRoute(NewsPage::LIST_URL);
        $I->see('News', NewsPage::MENU_ELEMENT);
        $I->see('Profile', NewsPage::MENU_ELEMENT);
        $I->see('Logout (', NewsPage::MENU_ELEMENT);
        $I->dontSee('Users', NewsPage::MENU_ELEMENT);
        $I->dontSee('Create Article', '.btn');
        $I->see('More', '.btn');
        $I->dontSee('Edit', '.btn');
        $I->dontSee('Remove', '.btn');

        $article = $I->grabFixture('news', 0);

        // ... and news contents
        $I->amOnRoute(NewsPage::VIEW_URL, ['id' => $article->id]);
        $I->see($article->title, NewsPage::ARTICLE_VIEW_TITLE_ELEMENT);
        $I->dontSee('Edit', '.btn');
        $I->dontSee('Remove', '.btn');
    }

    public function asModerator(\FunctionalTester $I)
    {
        $I->amLoggedInAs(self::MODERATOR);
        $this->_visitRoutes($I, 'moderator');

        // I see views previews ...
        $I->amOnRoute(NewsPage::LIST_URL);
        $I->see('News', NewsPage::MENU_ELEMENT);
        $I->see('Profile', NewsPage::MENU_ELEMENT);
        $I->see('Logout (', NewsPage::MENU_ELEMENT);
        $I->dontSee('Users', NewsPage::MENU_ELEMENT);

        $I->see('Create Article', '.btn');
        $I->see('More', '.btn');
        $I->see('Edit', '.btn');
        $I->see('Remove', '.btn');

        $article = $I->grabFixture('news', 0);

        // ... and news contents
        $I->amOnRoute(NewsPage::VIEW_URL, ['id' => $article->id]);
        $I->see($article->title, NewsPage::ARTICLE_VIEW_TITLE_ELEMENT);
        $I->see('Edit', '.btn');
        $I->see('Remove', '.btn');
    }

    public function asAdmin(\FunctionalTester $I)
    {
        $I->amLoggedInAs(self::ADMIN);
        $this->_visitRoutes($I, 'admin');

        // I see views previews ...
        $I->amOnRoute(NewsPage::LIST_URL);
        $I->see('News', NewsPage::MENU_ELEMENT);
        $I->see('Profile', NewsPage::MENU_ELEMENT);
        $I->see('Logout (', NewsPage::MENU_ELEMENT);
        $I->see('Users', NewsPage::MENU_ELEMENT);

        $I->see('Create Article', '.btn');
        $I->see('More', '.btn');
        $I->see('Edit', '.btn');
        $I->see('Remove', '.btn');

        $article = $I->grabFixture('news', 0);

        // ... and news contents
        $I->amOnRoute(NewsPage::VIEW_URL, ['id' => $article->id]);
        $I->see($article->title, NewsPage::ARTICLE_VIEW_TITLE_ELEMENT);
        $I->see('Edit', '.btn');
        $I->see('Remove', '.btn');

        // ... and users
        $I->amOnRoute(UsersPage::LIST_URL);
        $I->see('Create User', '.btn');
        $I->seeElement(UsersPage::GRID_ELEMENT);


        $user = $I->grabFixture('users', 0);

        $I->amOnRoute(UsersPage::EDIT_URL, ['id' => $user->id]);
        $I->see($user->name, 'h1');
        $I->seeElement(UsersPage::EDIT_FORM);
        $I->seeElement(UsersPage::RESET_PASSWORD_BTN);

        $I->amOnRoute(UsersPage::CREATE_URL);
        $I->seeElement(UsersPage::EDIT_FORM);
    }
}