<?php
/**
 *
 * User: psi
 * Date: 19.01.17
 * Time: 22:21
 */

namespace app\tests\functional;


use app\models\User;
use app\models\UserActivationCode;
use app\tests\fixtures\RealEventNotificationsFixture;
use app\tests\fixtures\UserFixture;
use yii\mail\MessageInterface;

class ChangePasswordCest
{
    public function _fixtures()
    {
        return [
            'users' => UserFixture::class,
            RealEventNotificationsFixture::class,
        ];
    }

    public function resetPassword(\FunctionalTester $I)
    {
        $user = $I->grabFixture('users', 0);
        $I->resetPasswordAs(100);

        // activation code was generated
        $uac = $I->grabRecord(UserActivationCode::class, [
            'user_id' => 100,
            'type' => UserActivationCode::TYPE_RESET_PASSWORD,
            'is_used' => 0
        ]);

        /** @var MessageInterface $email */
        $message = $I->grabLastSentEmail();
        $I->assertEquals([$user->email => $user->name], $message->getTo());
        $I->assertEquals('New password', $message->getSubject());
    }

    public function changePasswordWithWrongCode(\FunctionalTester $I)
    {
        $I->haveRecord(UserActivationCode::class, [
            'created_at' => '2017-01-01 00:00:00',
            'active_to' => '2999-01-01 00:00:00',
            'user_id' => 100,
            'is_used' => 0,
            'code' => '123',
        ]);

        $I->amOnRoute('profile/change-password', ['code' => uniqid()]);
        $I->see('Wrong activation code.');
    }

    public function changePasswordWithExpiredCode(\FunctionalTester $I)
    {
        $I->haveRecord(UserActivationCode::class, [
            'created_at' => '2017-01-01 00:00:00',
            'active_to' => '2017-01-01 00:00:00',
            'user_id' => 100,
            'is_used' => 0,
            'code' => '123',
        ]);

        $I->amOnRoute('profile/change-password', ['code' => '123']);
        $I->see('Wrong activation code.');
    }

    public function changePasswordWithUsedCode(\FunctionalTester $I)
    {
        $I->haveRecord(UserActivationCode::class, [
            'created_at' => '2017-01-01 00:00:00',
            'active_to' => '2999-01-01 00:00:00',
            'user_id' => 100,
            'is_used' => 1,
            'code' => '123',
        ]);

        $I->amOnRoute('profile/change-password', ['code' => '123']);
        $I->see('Wrong activation code.');
    }

    public function changePasswordWithCorrectCodeAsGuest(\FunctionalTester $I)
    {
        $user = User::findOne(100);

        $I->haveRecord(UserActivationCode::class, [
            'created_at' => '2017-01-01 00:00:00',
            'active_to' => '2999-01-01 00:00:00',
            'type' => UserActivationCode::TYPE_RESET_PASSWORD,
            'user_id' => 100,
            'is_used' => 0,
            'code' => '123',
        ]);

        $I->amOnRoute('profile/change-password', ['code' => '123']);
        $I->seeElement('form#change-password-form');
        $I->submitForm('form#change-password-form', [
            'ChangePasswordForm[password]' => '123456',
            'ChangePasswordForm[password_repeat]' => '123456'
        ]);

        $I->see('Your password was changed!');
        $I->see('Login');
        $I->login($user->email, '123456');
    }
}