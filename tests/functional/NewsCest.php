<?php
/**
 *
 * User: psi
 * Date: 21.01.17
 * Time: 16:15
 */

namespace app\tests\functional;


use app\models\News;
use app\models\User;
use app\tests\fixtures\AuthAssignmentFixture;
use app\tests\fixtures\NewsFixture;
use app\tests\fixtures\RealEventNotificationsFixture;
use app\tests\fixtures\UserFixture;

class NewsCest
{
    public function _fixtures()
    {
        return [
            'news' => NewsFixture::class,
            'users' => UserFixture::class,
            AuthAssignmentFixture::class,
            RealEventNotificationsFixture::class,
        ];
    }

    public function viewNews(\FunctionalTester $I)
    {
        \Yii::$app->params['newsPreviewsCount'] = 5;

        $I->amLoggedInAs(100);
        $I->amOnRoute('news/index');
        $I->seeElement('.news-list');
        $I->see('Create Article', '.btn');
        $I->seeElement('.news-content');
        $I->see('More', '.btn');
        $I->see('Edit', '.btn');
        $I->see('Remove', '.btn');
        $I->seeElement('.pagination');
    }

    public function viewArticle(\FunctionalTester $I)
    {
        $article = $I->grabRecord(News::class, ['id' => 100]);

        $I->amLoggedInAs(100);
        $I->amOnRoute('news/view', ['id' => 100]);
        $I->see($article->title);
        $I->see($article->body);
        $I->seeElement('.btn-news-edit');
        $I->seeElement('.btn-news-remove');
    }

    public function editArticle(\FunctionalTester $I)
    {
        $newTitle = 'Edit Title - ' . uniqid();
        $newBody = 'Edit Body - ' . uniqid();

        $article = $I->grabRecord(News::class, ['id' => 100]);

        $I->amLoggedInAs(100);
        $I->amOnRoute('news/edit', ['id' => 100]);
        $I->see($article->title, 'h1');
        $I->seeElement('.news-form form');
        $I->submitForm('.news-form form', [
            'News[title]' => $newTitle,
            'News[body]' => $newBody,
        ]);

        $I->seeInTitle($newTitle);
        $I->see($newTitle);
        $I->see($newBody);
    }

    public function editArticleWithIncorrectData(\FunctionalTester $I)
    {
        $I->amLoggedInAs(100);
        $I->amOnRoute('news/edit', ['id' => 100]);
        $I->submitForm('.news-form form', [
            'News[title]' => '',
            'News[body]' => '',
        ]);

        $I->see('Title cannot be blank.');
        $I->see('Contents cannot be blank.');

        $I->submitForm('.news-form form', [
            'News[title]' => '',
            'News[body]' => '111',
        ]);

        $I->see('Contents should contain at least 10 characters.');
    }

    public function canNotRemoveArticleByGET(\FunctionalTester $I)
    {
        $I->amLoggedInAs(100);
        $I->amOnRoute('news/remove', ['id' => 100]);
        $I->see('Method Not Allowed (#405)');
    }

    public function createArticle(\FunctionalTester $I)
    {
        $I->amLoggedInAs(100);
        $user = $I->grabRecord(User::class, ['id' => 100]);

        $newTitle = 'Create Title - ' . uniqid();
        $newBody = 'Create Body - ' . uniqid();

        $I->amOnRoute('news/create');
        $I->see('Create Article', 'h1');
        $I->seeElement('.news-form form');
        $I->seeElement('.news-form form input#news-title');
        $I->seeElement('.news-form form textarea#news-body');
        $I->submitForm('.news-form form', [
            'News[title]' => $newTitle,
            'News[body]' => $newBody,
        ]);

        $I->seeInTitle($newTitle);
        $I->see($newTitle);
        $I->see($newBody);
        $I->see($user->name, '.news-body p');

        $emails = $I->grabSentEmails();
        $aboutNews = 0;
        $recp = [];
        for ($i = 0; $i < 5; $i++) {
            $u = $I->grabFixture('users', $i);
            if ($u->is_allow_emails) {
                $recp[] = $u->email;
            }
        }
        foreach ($emails as $message) {
            $subj = $message->getSubject();
            if ($subj === 'New article were added!') {
                $aboutNews++;
                list($email, $name) = each($message->getTo());
                $I->assertTrue(in_array($email, $recp));
            }
        }
        expect($aboutNews)->greaterOrEquals(count($recp));
    }

    public function createArticleWithIncorrectData(\FunctionalTester $I)
    {
        $I->amLoggedInAs(100);
        $I->amOnRoute('news/create');
        $I->submitForm('.news-form form', [
            'News[title]' => '',
            'News[body]' => '',
        ]);

        $I->see('Title cannot be blank.');
        $I->see('Contents cannot be blank.');

        $I->submitForm('.news-form form', [
            'News[title]' => '',
            'News[body]' => '111',
        ]);

        $I->see('Contents should contain at least 10 characters.');
    }
}