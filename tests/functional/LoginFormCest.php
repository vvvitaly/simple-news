<?php

use app\tests\fixtures\UserFixture;

class LoginFormCest
{
    public function _fixtures()
    {
        return ['users' => UserFixture::class];
    }

    public function _before(\FunctionalTester $I)
    {
        $I->amOnRoute('site/login');
    }

    public function openLoginPage(\FunctionalTester $I)
    {
        $I->seeInTitle('Login');
    }

    // demonstrates `amLoggedInAs` method
    public function internalLoginById(\FunctionalTester $I)
    {
        $user = $I->grabFixture('users', 0);

        $I->amLoggedInAs($user->id);
        $I->amOnPage('/');
        $I->see('Logout (' . $user->name . ')');
    }

    public function loginWithEmptyCredentials(\FunctionalTester $I)
    {
        $I->submitForm('#login-form', []);
        $I->expectTo('see validations errors');
        $I->see('Email cannot be blank.');
        $I->see('Password cannot be blank.');
    }

    public function loginWithWrongCredentials(\FunctionalTester $I)
    {
        $I->submitForm('#login-form', [
            'LoginForm[email]' => 'some-wrong-email@some-domain.loc',
            'LoginForm[password]' => 'wrong-pass',
        ]);
        $I->expectTo('see validations errors');
        $I->see('Incorrect email or password.');
    }

    public function loginSuccessfully(\FunctionalTester $I)
    {
        $user = $I->grabFixture('users', 0);

        $I->submitForm('#login-form', [
            'LoginForm[email]' => $user->email,
            'LoginForm[password]' => 'test123',
        ]);
        $I->see('Logout (' . $user->name . ')');
        $I->dontSeeElement('form#login-form');              
    }
}