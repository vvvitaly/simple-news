<?php
/**
 *
 * User: psi
 * Date: 21.01.17
 * Time: 19:03
 */

namespace app\tests\functional;


use app\models\User;
use pages\UsersPage;
use app\models\UserActivationCode;
use app\tests\fixtures\UserFixture;

class UsersCest
{
    const USER_LOGGED_IN = 100;

    public function _fixtures()
    {
        return ['users' => UserFixture::class];
    }

    public function canNotViewUsersAsGuest(\FunctionalTester $I)
    {
        $I->amOnRoute(UsersPage::LIST_URL);
        $I->seeInTitle('Login');

        $I->amOnRoute(UsersPage::EDIT_URL, ['id' => 101]);
        $I->seeInTitle('Login');

        $I->amOnRoute(UsersPage::CREATE_URL);
        $I->seeInTitle('Login');

        $I->amOnRoute(UsersPage::RESET_PASSWORD_URL);
        $I->seeInTitle('Login');
    }

    public function viewUsers(\FunctionalTester $I)
    {
        $I->amLoggedInAs(self::USER_LOGGED_IN);
        $I->amOnRoute(UsersPage::LIST_URL);
        $I->seeInTitle('Users');
        $I->see('Create User', '.btn');
    }

    public function editUser(\FunctionalTester $I)
    {
        $I->amLoggedInAs(self::USER_LOGGED_IN);
        $uniq = uniqid('test-email-');
        $email = "$uniq@exampel.com";

        $I->amOnRoute(UsersPage::EDIT_URL, ['id' => 101]);
        $I->seeElement(UsersPage::EDIT_FORM);
        $I->submitForm(UsersPage::EDIT_FORM, [
            'User[email]' => $email,
            'User[name]' => $uniq,
        ]);

        $I->seeInTitle('Users');
        $I->see($email, UsersPage::gridCellByUserId(101, 'email'));
        $I->see($uniq, UsersPage::gridCellByUserId(101, 'name'));
    }

    public function editUserWithEmptyData(\FunctionalTester $I)
    {
        $I->amLoggedInAs(self::USER_LOGGED_IN);
        $I->amOnRoute(UsersPage::EDIT_URL, ['id' => 101]);
        $I->seeElement(UsersPage::EDIT_FORM);
        $I->submitForm(UsersPage::EDIT_FORM, [
            'User[email]' => '',
            'User[name]' => '',
        ]);
        $I->see('Email cannot be blank', UsersPage::errorElement('email'));
        $I->see('Name cannot be blank', UsersPage::errorElement('name'));
    }

    public function editUserWithWrongData(\FunctionalTester $I)
    {
        $I->amLoggedInAs(self::USER_LOGGED_IN);
        $I->amOnRoute(UsersPage::EDIT_URL, ['id' => 101]);
        $I->seeElement(UsersPage::EDIT_FORM);
        $I->submitForm(UsersPage::EDIT_FORM, [
            'User[email]' => 'email',
            'User[name]' => 'test',
        ]);
        $I->see('Email is not a valid email address.', UsersPage::errorElement('email'));
    }

    public function editUserWithExistsEmail(\FunctionalTester $I)
    {
        $user = $I->grabFixture('users', 0);

        $I->amLoggedInAs(self::USER_LOGGED_IN);
        $I->amOnRoute(UsersPage::EDIT_URL, ['id' => 101]);
        $I->seeElement(UsersPage::EDIT_FORM);
        $I->submitForm(UsersPage::EDIT_FORM, [
            'User[email]' => $user->email,
            'User[name]' => 'test',
        ]);
        $I->see('Email "' . $user->email . '" has already been taken.', UsersPage::errorElement('email'));
    }

    public function editUserWrongUser(\FunctionalTester $I)
    {
        $I->amLoggedInAs(self::USER_LOGGED_IN);
        $I->amOnRoute(UsersPage::EDIT_URL, ['id' => 0]);
        $I->see('Not Found (#404)');
    }

    public function testResetPassword(\FunctionalTester $I)
    {
        $I->amLoggedInAs(self::USER_LOGGED_IN);
        $I->amOnRoute(UsersPage::EDIT_URL, ['id' => 101]);
        $I->submitForm('form#reset-password-form', []);
        $I->seeElement(UsersPage::EDIT_FORM);
        $I->see('The password was reset.');

        $I->seeRecord(UserActivationCode::class, [
            'user_id' => 101,
            'type' => UserActivationCode::TYPE_RESET_PASSWORD,
            'is_used' => 0,
        ]);
    }

    public function canNotResetPasswordByGET(\FunctionalTester $I)
    {
        $I->amLoggedInAs(self::USER_LOGGED_IN);
        $I->amOnRoute(UsersPage::RESET_PASSWORD_URL, ['id' => 101]);
        $I->see('Method Not Allowed (#405)');
    }

    public function canNotRemoveArticleByGET(\FunctionalTester $I)
    {
        $I->amLoggedInAs(self::USER_LOGGED_IN);
        $I->amOnRoute(UsersPage::REMOVE_URL, ['id' => 101]);
        $I->see('Method Not Allowed (#405)');
    }

    public function createNewUser(\FunctionalTester $I)
    {
        $I->amLoggedInAs(self::USER_LOGGED_IN);
        $uniq = uniqid('test-email-');
        $email = "$uniq@exampel.com";

        $I->amOnRoute(UsersPage::CREATE_URL);
        $I->seeElement(UsersPage::EDIT_FORM);
        $I->submitForm(UsersPage::EDIT_FORM, [
            'User[email]' => $email,
            'User[name]' => $uniq,
            'User[role]' => 'user',
        ]);

        $newUser = $I->grabRecord(User::class, ['email' => $email]);

        $I->seeInTitle('Users');
        $I->see($email, UsersPage::gridCellByUserId($newUser->id, 'email'));
        $I->see($uniq, UsersPage::gridCellByUserId($newUser->id, 'name'));

        $user = $I->grabRecord(User::class, ['email' => $email]);
        // activation code was created
        $I->seeRecord(UserActivationCode::class, [
            'user_id' => $user->id,
            'type' => UserActivationCode::TYPE_ACTIVATE_ACCOUNT,
            'is_used' => 0,
        ]);
    }

    public function createNewUserWithEmptyData(\FunctionalTester $I)
    {
        $I->amLoggedInAs(self::USER_LOGGED_IN);
        $I->amOnRoute(UsersPage::CREATE_URL);
        $I->seeElement(UsersPage::EDIT_FORM);
        $I->submitForm(UsersPage::EDIT_FORM, [
            'User[email]' => '',
            'User[name]' => '',
        ]);
        $I->see('Email cannot be blank', UsersPage::errorElement('email'));
        $I->see('Name cannot be blank', UsersPage::errorElement('name'));
    }

    public function createNewUserWithWrongData(\FunctionalTester $I)
    {
        $I->amLoggedInAs(self::USER_LOGGED_IN);
        $I->amOnRoute(UsersPage::CREATE_URL);
        $I->seeElement(UsersPage::EDIT_FORM);
        $I->submitForm(UsersPage::EDIT_FORM, [
            'User[email]' => 'email',
            'User[name]' => 'test',
        ]);
        $I->see('Email is not a valid email address.', UsersPage::errorElement('email'));
    }

    public function createNewUserWithExistsEmail(\FunctionalTester $I)
    {
        $user = $I->grabFixture('users', 0);

        $I->amLoggedInAs(self::USER_LOGGED_IN);
        $I->amOnRoute(UsersPage::CREATE_URL);
        $I->seeElement(UsersPage::EDIT_FORM);
        $I->submitForm(UsersPage::EDIT_FORM, [
            'User[email]' => $user->email,
            'User[name]' => 'test',
        ]);
        $I->see('Email "' . $user->email . '" has already been taken.', UsersPage::errorElement('email'));
    }
}