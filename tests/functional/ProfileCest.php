<?php
/**
 *
 * User: psi
 * Date: 19.01.17
 * Time: 22:28
 */

namespace app\tests\functional;


class ProfileCest
{
    public function canNotOpenProfileIfGuest(\FunctionalTester $I)
    {
        $I->amOnRoute('/profile');
        $I->seeInTitle('Login');
        $I->seeElement('form#login-form');
        $I->dontSee('Reset Password');
    }

    public function canNotResetPasswordViaGET(\FunctionalTester $I)
    {
        $I->amLoggedInAs(100);
        $I->amOnRoute('profile/reset-password');
        $I->seeInTitle('Method Not Allowed');
    }
}