<?php
/**
 *
 * User: psi
 * Date: 25.01.17
 * Time: 2:02
 */

namespace app\tests\fixtures;


use app\models\EventNotification;
use yii\test\ActiveFixture;

class RealEventNotificationsFixture extends ActiveFixture
{
    public $modelClass = EventNotification::class;

    public function init()
    {
        parent::init();
        $this->dataFile = __DIR__ . '/data/real_events_notifications.php';
    }
}