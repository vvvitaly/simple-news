<?php
/**
 *
 * User: psi
 * Date: 25.01.17
 * Time: 2:03
 */

use app\models\News;
use app\models\User;

return [
    [
        'target_class' => User::class,
        'target_event' => User::EVENT_SIGNUP_COMPLETE,
        'transport_name' => 'email',
        'recipient_type' => 'owner',
        'content_template' => json_encode([
            'subject' => 'Activate your account',
            'body' => 'Hi <b>{{sender.name}}</b>! Welcome to our super-duper news portal. Please activate your account <a href="{{sender.currentAccountActivationCode.link}}">here</a>'
        ])
    ],
    [
        'target_class' => User::class,
        'target_event' => User::EVENT_RESET_PASSWORD,
        'transport_name' => 'email',
        'recipient_type' => 'owner',
        'content_template' => json_encode([
            'subject' => 'New password',
            'body' => 'Hi <b>{{sender.name}}</b>! Change your password <a href="{{sender.currentResetPasswordCode.link}}">here</a>'
        ])
    ],
    [
        'target_class' => News::class,
        'target_event' => News::EVENT_AFTER_INSERT,
        'transport_name' => 'email',
        'recipient_type' => 'all',
        'content_template' => json_encode([
            'subject' => 'New article were added!',
            'body' => 'Hi! {{sender.user.name}} added new <a href="{{sender.link}}">article</a> "{{sender.title}}"'
        ])
    ]
];