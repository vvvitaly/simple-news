<?php

return [
    [
        'id' => 100,
        'created_at' => '2015-09-07 15:45:50',
        'email' => 'nader.karley@gmail.com',
        'password_hash' => '$2y$13$J/c/4OX.ZSWMgSiwF3lhMulGgNswPFVVD5P2LoZJS7UCYgE05Wzx2',
        'auth_key' => 'lDNywTS3XbvCPYRVtBdLJeMCJ7UORFkv',
        'name' => 'Dameon Windler',
        'is_activated' => 1,
        'is_allow_emails' => 1,
    ],
    [
        'id' => 101,
        'created_at' => '2015-09-17 16:54:59',
        'email' => 'mhilll@larson.com',
        'password_hash' => '$2y$13$bd0jyaeUvKww4n9HY1p/ueGgF1F3EmakI6iK6rSG0b3QbH0pu1DDe',
        'auth_key' => 'ECiITg0XuZc00qo02zM41X07qtxT3zfJ',
        'name' => 'Adriel Powlowski',
        'is_activated' => 1,
        'is_allow_emails' => 0,
    ],
    [
        'id' => 102,
        'created_at' => '2015-12-31 12:54:14',
        'email' => 'kunze.caesar@schmidt.org',
        'password_hash' => '$2y$13$bm9E64jfjDNRliTpanzbT.B0llfIaxfOTdSMwwlubq7MHLBj8rDF.',
        'auth_key' => '0qxwdtfJPm8gaBI3rVNXCSo_1p2q8aM1',
        'name' => 'Maximilian Schuppe IV',
        'is_activated' => 1,
        'is_allow_emails' => 1,
    ],
    [
        'id' => 103,
        'created_at' => '2016-03-30 11:09:25',
        'email' => 'gerry03@wiza.com',
        'password_hash' => '$2y$13$jFjMUeZJ3hO1rwJSwtQFZObqn/w1YiUnzgxtibTYaJPGGmNCrGb6O',
        'auth_key' => 'k5C6W3TXK7SnPIUOwxUOj39L8iwxnhhG',
        'name' => 'Miss Jody Beatty II',
        'is_activated' => 0,
        'is_allow_emails' => 1,
    ],
    [
        'id' => 104,
        'created_at' => '2014-11-24 00:50:09',
        'email' => 'dpfeffer@jacobson.biz',
        'password_hash' => '$2y$13$FlFTpiantm5pR9kDUNFiHOwN4cYyaug5GwO5YHSur7RKEHx5VXWN6',
        'auth_key' => 'ikfBsm9esev_ZKfYF62jgybpLpzxNMsG',
        'name' => 'Mr. Zane Wyman IV',
        'is_activated' => 1,
        'is_allow_emails' => 1,
    ],
];
