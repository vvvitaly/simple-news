<?php
/**
 *
 * User: psi
 * Date: 22.01.17
 * Time: 22:51
 */

return [
    // TODO: maker common target stub
    'target1-event1-all' => [
        'id' => 100,
        'transport_name' => 'dr1',
        'target_class' => 'app\tests\unit\events\_DbRouterTest_TargetClass1',
        'target_event' => 'event1',
        'recipient_value' => null,
        'recipient_type' => 'all',
    ],
    '*-event1-owner' => [
        'id' => 101,
        'transport_name' => 'dr2',
        'target_class' => '*',
        'target_event' => 'event1',
        'recipient_value' => null,
        'recipient_type' => 'owner',
    ],
    '*-event1-role-users' => [
        'id' => 102,
        'transport_name' => 'dr3',
        'target_class' => '*',
        'target_event' => 'event1',
        'recipient_type' => 'role',
        'recipient_value' => 'user',
    ],
    '*-all' => [
        'id' => 103,
        'transport_name' => 'dr4',
        'target_class' => '*',
        'target_event' => '*',
        'recipient_type' => 'all',
        'recipient_value' => null,
        'content_template' => 'Some template',
    ],
    '*-group' => [
        'id' => 104,
        'transport_name' => 'dr4',
        'target_class' => '*',
        'target_event' => 'some-group-event',
        'recipient_type' => 'group',
        'recipient_value' => null,
        'content_template' => 'Some template',
    ],
];