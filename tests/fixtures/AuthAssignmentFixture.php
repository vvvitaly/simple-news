<?php
/**
 *
 * User: psi
 * Date: 22.01.17
 * Time: 16:51
 */

namespace app\tests\fixtures;


use yii\test\ActiveFixture;

class AuthAssignmentFixture extends ActiveFixture
{
    public $tableName = 'auth_assignment';
    public $depends = [UserFixture::class];
}