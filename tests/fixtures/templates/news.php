<?php
/**
 *
 * User: psi
 * Date: 21.01.17
 * Time: 11:00
 */
use yii\helpers\ArrayHelper;

/**
 * @var $faker \Faker\Generator
 * @var $index integer
 *
 */

$users = ArrayHelper::getColumn(require (__DIR__ . '/../data/users.php'), 'id');

return [
    'id' => 100 + $index,
    'created_at' => $faker->dateTimeBetween('-3 years')->format('Y-m-d H:i:s'),
    'user_id' => $faker->randomElement($users),
    'title' => $faker->sentence($faker->numberBetween(2, 5)),
    'body' => $faker->realText($faker->numberBetween(500, 3000)),
];
