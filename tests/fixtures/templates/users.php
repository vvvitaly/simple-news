<?php
/**
 *
 * User: psi
 * Date: 16.01.17
 * Time: 23:31
 */

/**
 * @var $faker \Faker\Generator
 * @var $index integer
 *
 */

return [
    'id' => 100 + $index,
    'created_at' => $faker->dateTimeBetween('-3 years')->format('Y-m-d H:i:s'),
    'email' => $faker->email,
    'password_hash' => Yii::$app->security->generatePasswordHash('test123'),
    'auth_key' => Yii::$app->security->generateRandomString(),
    'name' => $faker->name,
    'is_activated' => (int)$faker->boolean(70),
];
