<?php
/**
 *
 * User: psi
 * Date: 17.01.17
 * Time: 19:42
 */

namespace app\tests\fixtures;


use app\models\User;
use yii\test\ActiveFixture;

class UserFixture extends ActiveFixture
{
    public $modelClass = User::class;
}