<?php
/**
 *
 * User: psi
 * Date: 22.01.17
 * Time: 22:51
 */

namespace app\tests\fixtures;


use app\models\EventNotification;
use yii\test\ActiveFixture;

class EventNotificationsFixture extends ActiveFixture
{
    public $modelClass = EventNotification::class;
}