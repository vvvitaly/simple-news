<?php
/**
 *
 * User: psi
 * Date: 21.01.17
 * Time: 11:09
 */

namespace app\tests\fixtures;


use app\models\News;
use yii\test\ActiveFixture;

class NewsFixture extends ActiveFixture
{
    public $modelClass = News::class;
    public $depends = [UserFixture::class];
}