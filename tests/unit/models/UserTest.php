<?php

namespace tests\models;

use app\auth\Permissions;
use app\models\ActivationEvent;
use app\models\User;
use app\models\UserActivationCode;
use app\tests\fixtures\AuthAssignmentFixture;
use app\tests\fixtures\UserFixture;
use Codeception\Test\Unit;
use yii\helpers\ArrayHelper;

class UserTest extends Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
        parent::_before();
        $this->tester->haveFixtures([
            'users' => UserFixture::class,
            AuthAssignmentFixture::class,
        ]);
    }

    public function testGenerateAuthKey()
    {
        $user = new User();
        expect($user->auth_key)->isEmpty();

        $user->generateAuthKey();
        expect(strlen($user->auth_key))->equals(32);
    }

    public function testValidateAuthKey()
    {
        $user = new User();
        $user->auth_key = '123';

        expect_that($user->validateAuthKey('123'));
        expect_not($user->validateAuthKey('qwerty'));
    }

    public function testSetPassword()
    {
        $user = new User();
        expect($user->password_hash)->isEmpty();

        $user->setPassword('123');
        expect($user->password_hash)->notEmpty();
        expect_that($user->validatePassword('123'));
        expect_not($user->validatePassword('321'));
    }

    public function testShouldGenerateAuthKeyWhenCreate()
    {
        $user = new User();
        $user->created_at = date('Y-m-d H:i:s');
        $user->email = 'test@localhost.dev';
        $user->name = 'test';
        $user->password = '123456';

        expect_that($user->save());
        expect($user->auth_key)->notEmpty();
        $expectedAuthKey = $user->auth_key;

        $user->email = 'test2@localhost.dev';
        $user->save();

        $actualUser = User::findOne($user->id);
        expect($actualUser->email)->equals('test2@localhost.dev');
        expect($actualUser->auth_key)->equals($expectedAuthKey);
    }

    public function testShouldNotBeActivatedByDefault()
    {
        $user = new User();
        $user->created_at = date('Y-m-d H:i:s');
        $user->email = 'test@localhost.dev';
        $user->name = 'test';
        $user->password = '123456';

        expect_that($user->save());
        expect($user->is_activated)->equals(0);
    }

    public function testActivationFlagShouldNotReset()
    {
        $user = new User();
        $user->created_at = date('Y-m-d H:i:s');
        $user->email = 'test@localhost.dev';
        $user->name = 'test';
        $user->password = '123456';
        $user->is_activated = true;

        expect_that($user->save());
        expect($user->is_activated)->true();
    }

    public function testSignupShouldTriggerEvent()
    {
        $user = new User();
        $called = false;
        $h = function($e) use (&$called, $user) {
            $called = true;
            expect($e->sender)->same($user);
        };

        $user->on(User::EVENT_SIGNUP_COMPLETE, $h);
        $user->created_at = date('Y-m-d H:i:s');
        $user->email = uniqid('test-', true) . '@localhost.dev';
        $user->name = 'test';
        $user->password = '123456';
        $saved = $user->signup();

        expect($saved)->true();
        expect($called)->true();
    }

    public function testSignupShouldNotTriggerEventIfFailSaving()
    {
        $user = new User();
        $called = false;
        $h = function($e) use (&$called, $user) {
            $called = true;
        };

        $user->on(User::EVENT_SIGNUP_COMPLETE, $h);
        $saved = $user->signup();

        expect($saved)->false();
        expect($called)->false();
    }

    /**
     * @expectedException \app\models\SignupException
     */
    public function testSignupShouldRaiseErrorIfUpdate()
    {
        $user = User::findOne(100);
        $user->signup();
    }

    public function testSignupShouldAssignUserRole()
    {
        $user = new User();
        $user->created_at = date('Y-m-d H:i:s');
        $user->email = uniqid('test-', true) . '@localhost.dev';
        $user->name = 'test';
        $user->password = '123456';
        $saved = $user->signup();

        expect($saved)->true();

        $roles = ArrayHelper::getColumn(\Yii::$app->authManager->getRolesByUser($user->id), 'name', false);
        expect($roles)->equals(['user']);
    }

    public function testActivate()
    {
        // not activated
        $user = User::findOne(103);

        $uac = UserActivationCode::generateForUser($user, UserActivationCode::TYPE_ACTIVATE_ACCOUNT);
        expect_that($uac->id);
        $uac->refresh();

        $eventObj = null;
        $h = function($e) use (&$eventObj) {
            $eventObj = $e;
        };
        $user->on(User::EVENT_ACTIVATED, $h);
        $saved = $user->activate($uac);
        expect_that($saved);
        $user->refresh();
        $uac->refresh();

        expect_that($user->is_activated);
        expect_that($uac->is_used);

        // assert event triggered
        expect($eventObj)->notEmpty();
        expect($eventObj->sender)->same($user);
    }

    /**
     * @expectedException \yii\base\UserException
     */
    public function testActivateShouldRaiseExceptionIfWrongActivationUser()
    {
        // not activated
        $user = User::findOne(103);

        $uac = UserActivationCode::generateForUser(User::findOne(100), UserActivationCode::TYPE_ACTIVATE_ACCOUNT);
        expect_that($uac->id);

        $user->activate($uac);
    }

    /**
     * @expectedException \yii\base\UserException
     */
    public function testActivateShouldRaiseExceptionIfNotValidActivation()
    {
        // not activated
        $user = User::findOne(103);

        $uac = UserActivationCode::generateForUser($user, UserActivationCode::TYPE_ACTIVATE_ACCOUNT, null, false);
        $uac->is_used = true;
        $saved = $uac->save();
        expect_that($saved);

        $user->activate($uac);
    }

    /**
     * @expectedException \yii\base\UserException
     */
    public function testActivateShouldRaiseExceptionIfWrongType()
    {
        // not activated
        $user = User::findOne(103);

        $uac = UserActivationCode::generateForUser($user, UserActivationCode::TYPE_RESET_PASSWORD, null, false);
        $uac->is_used = true;
        $saved = $uac->save();
        expect_that($saved);

        $user->activate($uac);
    }

    /**
     * @expectedException \yii\base\UserException
     */
    public function testActivateShouldRaiseExceptionIfAlreadyActivated()
    {
        // activated
        $user = User::findOne(100);

        $uac = UserActivationCode::generateForUser($user, UserActivationCode::TYPE_ACTIVATE_ACCOUNT);
        expect_that($uac->id);

        $user->activate($uac);
    }

    public function testResetPasswordShouldTriggersEvent()
    {
        $user = User::findOne(100);

        $eventObj = null;
        $h = function($e) use (&$eventObj) {
            $eventObj = $e;
        };

        $user->on(User::EVENT_RESET_PASSWORD, $h);
        $uac = $user->resetPassword();

        expect($uac->id)->notEmpty();
        expect($eventObj)->notEmpty();
        expect($eventObj->sender)->same($user);
        expect($eventObj->activation)->notNull();
        expect($eventObj->activation->id)->notNull();
        expect($eventObj->activation->code)->notEmpty();
        expect($eventObj->activation->type)->equals(UserActivationCode::TYPE_RESET_PASSWORD);
        expect($eventObj->activation->user_id)->equals($user->id);
        expect($eventObj->activation->is_used)->equals(0);
    }

    public function testChangePassword()
    {
        // not activated
        $user = User::findOne(103);

        $uac = UserActivationCode::generateForUser($user, UserActivationCode::TYPE_RESET_PASSWORD);
        expect_that($uac->id);
        $uac->refresh();

        $eventObj = null;
        $h = function($e) use (&$eventObj) {
            $eventObj = $e;
        };

        $user->on(User::EVENT_CHANGE_PASSWORD, $h);
        $saved = $user->changePassword('qwerty', $uac);
        expect_that($saved);
        $user->refresh();
        $uac->refresh();

        // activated
        expect_that($uac->is_used);

        // assert event triggered
        expect($eventObj)->notEmpty();
        expect($eventObj->sender)->same($user);

        // password changed
        $user->validatePassword('qwerty');
    }

    /**
     * @expectedException \yii\base\UserException
     */
    public function testChangePasswordShouldRaiseExceptionIfWrongActivationUser()
    {
        $user = User::findOne(103);

        $uac = UserActivationCode::generateForUser(User::findOne(100), UserActivationCode::TYPE_RESET_PASSWORD);
        expect_that($uac->id);

        $user->changePassword('qwerty', $uac);
    }

    /**
     * @expectedException \yii\base\UserException
     */
    public function testChangePasswordShouldRaiseExceptionIfNotValidActivation()
    {
        $user = User::findOne(103);

        $uac = UserActivationCode::generateForUser($user, UserActivationCode::TYPE_ACTIVATE_ACCOUNT, null, false);
        $uac->is_used = true;
        $saved = $uac->save();
        expect_that($saved);

        $user->changePassword('qwerty', $uac);
    }

    /**
     * @expectedException \yii\base\UserException
     */
    public function testChangePasswordShouldRaiseExceptionIfWrongType()
    {
        $user = User::findOne(103);

        $uac = UserActivationCode::generateForUser($user, UserActivationCode::TYPE_ACTIVATE_ACCOUNT, null, false);
        $uac->is_used = true;
        $saved = $uac->save();
        expect_that($saved);

        $user->changePassword('qwerty', $uac);
    }

    public function testAssignRole()
    {
        $auth = \Yii::$app->authManager;

        $user = User::findOne(103);

        $user->assignRole(Permissions::ROLE_USER);
        $roles = array_keys($auth->getRolesByUser($user->id));
        expect($roles)->equals([Permissions::ROLE_USER]);

        $user->assignRole(Permissions::ROLE_ADMIN);
        $roles = array_keys($auth->getRolesByUser($user->id));
        expect($roles)->equals([Permissions::ROLE_ADMIN]);
    }
}
