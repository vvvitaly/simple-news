<?php
/**
 *
 * User: psi
 * Date: 17.01.17
 * Time: 21:59
 */

namespace app\tests\unit\models;


use app\models\SignupForm;
use app\models\User;
use app\tests\fixtures\EmptyEventNotificationFixture;
use app\tests\fixtures\UserFixture;
use Codeception\Test\Unit;
use Codeception\Util\Stub;

class SignUpFormTest extends Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var UserFixture
     */
    protected $_users;

    protected function _before()
    {
        parent::_before();
        $this->tester->haveFixtures(['users' => UserFixture::class, EmptyEventNotificationFixture::class]);
        $this->_users = $this->tester->grabFixture('users');
    }

    public function testValidation()
    {
        $I = $this->tester;

        $form = new SignupForm([]);

        $I->amGoingTo('validate form with empty attributes');
        $form->validate();
        expect('error in email, password, password_repeat fields', $form->errors)->count(3);
        expect('has error in email', $form->errors)->hasKey('email');
        expect('has error in password', $form->errors)->hasKey('password');
        expect('has error in password_repeat', $form->errors)->hasKey('password_repeat');

        $I->amGoingTo('validate form with empty string attributes');
        $form = new SignupForm(['email' => '', 'password' => '', 'password_repeat' => '', 'name' => '']);
        $form->validate();
        expect('error in email, password, password_repeat fields', $form->errors)->count(3);
        expect('has error in email', $form->errors)->hasKey('email');
        expect('has error in password', $form->errors)->hasKey('password');
        expect('has error in password_repeat', $form->errors)->hasKey('password_repeat');

        $I->amGoingTo('validate form with wrong email');
        $form = new SignupForm(['email' => 'test', 'password' => '123456', 'password_repeat' => '123456', 'name' => 'test']);
        $form->validate();
        expect('error in email field', $form->errors)->count(1);
        expect('has error in email', $form->errors)->hasKey('email');

        $I->amGoingTo('validate form with wrong password repeat');
        $form = new SignupForm(['email' => 'test@example.com', 'password' => '123456', 'password_repeat' => '654321', 'name' => 'test']);
        $form->validate();
        expect('error in password field', $form->errors)->count(1);
        expect('has error in password', $form->errors)->hasKey('password');

        $I->amGoingTo('validate form with correct fields');
        $form = new SignupForm(['email' => 'test@example.com', 'password' => '123456', 'password_repeat' => '123456', 'name' => '']);
        $form->validate();
        expect('no errors', $form->errors)->count(0);

        $I->amGoingTo('validate form with too short password');
        $form = new SignupForm(['email' => 'test@example.com', 'password' => '123', 'password_repeat' => '123', 'name' => '']);
        $form->validate();
        expect('error only in password field', $form->errors)->count(1);
        expect('has error in password', $form->errors)->hasKey('password');

        $I->amGoingTo('validate form with existing email');
        $form = new SignupForm(['email' => $this->_users->getModel(0)->email, 'password' => '123456', 'password_repeat' => '123456', 'name' => '']);
        $form->validate();
        expect('error only in email field', $form->errors)->count(1);
        expect('has error in email', $form->errors)->hasKey('email');
    }

    public function testGetName()
    {
        $form = new SignupForm(['email' => 'test@localhost']);
        expect($form->name)->equals('test');

        $form = new SignupForm(['email' => 'test']);
        expect($form->name)->equals('test');

        $form = new SignupForm(['email' => '']);
        expect($form->name)->equals('');

        $form = new SignupForm(['email' => 'test@localhost', 'name' => 'name']);
        expect($form->name)->equals('name');
    }

    public function testSignupWithWrongData()
    {
        $email = uniqid('test-', true) . '@example.com';
        $form = new SignupForm(['email' => $email]);

        $saved = $form->signup();
        expect($saved)->null();
        expect($form->errors)->hasKey('password');
    }

    public function testSignupWithCorrectData()
    {
        $email = uniqid('test-', true) . '@example.com';
        $form = new SignupForm([
            'email' => $email,
            'password' => '123456',
            'password_repeat' => '123456',
            'name' => 'test-name',
        ]);

        $user = $form->signup();
        expect($user)->notNull();

        $actual = User::findOne(['email' => $email]);
        expect($actual)->notNull();
        expect($actual->created_at)->notEmpty();
        expect($actual->password_hash)->notEmpty();
        expect($actual->auth_key)->notEmpty();
        expect($actual->name)->equals('test-name');
    }

    public function testSignupShouldCallUserSignup()
    {
        // mocke user
        $user = $this->getMockBuilder(User::class)->getMock();
        $user->expects($this->once())->method('signup')->willReturn(true);

        // mock signup form
        $form = $this->getMockBuilder(SignupForm::class)
            ->setConstructorArgs([[
                'email' => uniqid('test-', true) . '@example.com',
                'password' => '123456',
                'password_repeat' => '123456',
                'name' => 'test-name',
            ]])
            ->setMethods(['createUser'])
            ->getMock();
        $form->expects($this->once())->method('createUser')->willReturn($user);

        /** @var SignupForm $form */
        $actualUser = $form->signup();
        expect($actualUser)->same($user);
    }
}