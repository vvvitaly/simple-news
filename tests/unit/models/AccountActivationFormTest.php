<?php
/**
 *
 * User: psi
 * Date: 19.01.17
 * Time: 8:34
 */

namespace app\tests\unit\models;


use app\models\AccountActivationForm;
use app\models\User;
use app\models\UserActivationCode;
use Codeception\Test\Unit;

class AccountActivationFormTest extends Unit
{
    public function testActivate()
    {
        // not activated
        $user = User::findOne(103);

        $uac = UserActivationCode::generateForUser($user, UserActivationCode::TYPE_ACTIVATE_ACCOUNT);
        expect_that($uac->id);
        $uac->refresh();

        $form = new AccountActivationForm([
            'code' => $uac->code,
        ]);

        expect_that($form->activate());
        $user->refresh();
        $uac->refresh();

        expect_that($user->is_activated);
        expect_that($uac->is_used);
    }

    public function testValidateShouldRaiseExceptionIfWrongType()
    {
        // not activated
        $user = User::findOne(103);

        $uac = UserActivationCode::generateForUser($user, UserActivationCode::TYPE_RESET_PASSWORD);
        expect_that($uac->id);
        $uac->refresh();

        $form = new AccountActivationForm([
            'activationModel' => $uac,
        ]);


        expect_not($form->validate());
        expect($form->errors)->count(1);
        expect($form->errors)->hasKey('code');
    }

    public function testValidateShouldErrorIfUserAlreadyActivated()
    {
        // activated
        $user = User::findOne(100);

        $uac = UserActivationCode::generateForUser($user, UserActivationCode::TYPE_ACTIVATE_ACCOUNT);
        expect_that($uac->id);
        $uac->refresh();

        $form = new AccountActivationForm([
            'code' => $uac->code,
        ]);

        expect_not($form->validate());
        expect($form->errors)->count(1);
        expect($form->errors)->hasKey('code');
    }

    public function testValidateShouldErrorIfActivationCodeUsed()
    {
        // not activated
        $user = User::findOne(103);

        $uac = UserActivationCode::generateForUser($user, UserActivationCode::TYPE_ACTIVATE_ACCOUNT, null, false);
        $uac->is_used = true;
        $saved = $uac->save();
        expect_that($saved);
        $uac->refresh();

        $form = new AccountActivationForm([
            'code' => $uac->code,
        ]);

        expect_not($form->validate());
        expect($form->errors)->count(1);
        expect($form->errors)->hasKey('code');
    }

    public function testValidateShouldErrorIfActivationCodeExpires()
    {
        // not activated
        $user = User::findOne(103);

        $uac = UserActivationCode::generateForUser($user, UserActivationCode::TYPE_ACTIVATE_ACCOUNT, null, false);
        $uac->active_to = '2017-01-01 00:00:00';
        $saved = $uac->save();
        expect_that($saved);
        $uac->refresh();

        $form = new AccountActivationForm([
            'code' => $uac->code,
        ]);

        expect_not($form->validate());
        expect($form->errors)->count(1);
        expect($form->errors)->hasKey('code');
    }

    public function testValidateShouldErrorIfActivationCodeWrong()
    {
        $form = new AccountActivationForm([
            'code' => '000',
        ]);

        expect_not($form->validate());
        expect($form->errors)->count(1);
        expect($form->errors)->hasKey('code');
    }
}