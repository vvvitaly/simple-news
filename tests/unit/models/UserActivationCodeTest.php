<?php
/**
 *
 * User: psi
 * Date: 18.01.17
 * Time: 20:45
 */

namespace app\tests\unit\models;


use app\models\User;
use app\models\UserActivationCode;
use app\tests\fixtures\UserFixture;
use Codeception\Test\Unit;

class UserActivationCodeTest extends Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
        parent::_before();
        $this->tester->haveFixtures(['users' => UserFixture::class]);
    }

    public function testGenerateLink()
    {
        $expectedLifetimeDays = 3;

        $user = $this->tester->grabFixture('users', 0);

        $inst = UserActivationCode::generateForUser($user, UserActivationCode::TYPE_ACTIVATE_ACCOUNT);
        expect_that($inst->id);
        $inst->refresh();

        expect($inst->created_at)->notEmpty();
        expect(
            date_diff(new \DateTime($inst->active_to), new \DateTime($inst->created_at))->days
        )->equals($expectedLifetimeDays);
        expect($inst->type)->equals(UserActivationCode::TYPE_ACTIVATE_ACCOUNT);
        expect($inst->code)->notEmpty();
        expect($inst->user_id)->equals($user->id);
        expect($inst->is_used)->equals(false);
    }

    public function testGenerateLinkWithLifetime()
    {
        $expectedLifetimeDays = 1;

        $user = $this->tester->grabFixture('users', 0);

        $inst = UserActivationCode::generateForUser($user, UserActivationCode::TYPE_RESET_PASSWORD, $expectedLifetimeDays);
        expect_that($inst->id);
        $inst->refresh();

        expect($inst->created_at)->notEmpty();
        expect(
            date_diff(new \DateTime($inst->active_to), new \DateTime($inst->created_at))->days
        )->equals($expectedLifetimeDays);
        expect($inst->type)->equals(UserActivationCode::TYPE_RESET_PASSWORD);
        expect($inst->code)->notEmpty();
        expect($inst->user_id)->equals($user->id);
        expect($inst->is_used)->equals(false);
    }

    public function testApply()
    {
        $user = $this->tester->grabFixture('users', 0);
        $inst = UserActivationCode::generateForUser($user, UserActivationCode::TYPE_RESET_PASSWORD);
        expect_that($inst->id);

        $inst->apply();
        expect($inst->is_used)->true();
    }

    public function testIsValid()
    {
        $uac = new UserActivationCode([
            'created_at' => '2017-01-01 00:00:00',
            'active_to' => '2999-01-01 00:00:00',
            'user_id' => 100,
            'code' => '123',
            'type' => UserActivationCode::TYPE_ACTIVATE_ACCOUNT,
            'is_used' => false,
        ]);

        expect_that($uac->isValid());

        $uac->active_to = '2017-01-01 00:00:00';
        expect_not($uac->isValid());

        $uac->active_to = '2999-01-01 00:00:00';
        $uac->is_used = true;
        expect_not($uac->isValid());
    }

    public function testGenerateShoudldDeactivateOldCodes()
    {
        $user = $this->tester->grabFixture('users', 0);
        $user2 = $this->tester->grabFixture('users', 0);

        UserActivationCode::generateForUser($user2, UserActivationCode::TYPE_ACTIVATE_ACCOUNT);
        UserActivationCode::generateForUser($user, UserActivationCode::TYPE_ACTIVATE_ACCOUNT);
        UserActivationCode::generateForUser($user, UserActivationCode::TYPE_RESET_PASSWORD);
        $lastInst = UserActivationCode::generateForUser($user, UserActivationCode::TYPE_ACTIVATE_ACCOUNT);

        $actual = UserActivationCode::find()->where([
            'type' => UserActivationCode::TYPE_ACTIVATE_ACCOUNT,
            'user_id' => $user->id,
            'is_used' => 0,
        ])->all();

        expect($actual)->count(1);
        expect($actual[0]->id)->equals($lastInst->id);

        expect(UserActivationCode::findAll([
            'user_id' => $user2->id,
            'type' => UserActivationCode::TYPE_ACTIVATE_ACCOUNT,
            'is_used' => 0,
        ]))->count(1);

        expect(UserActivationCode::findAll([
            'user_id' => $user->id,
            'type' => UserActivationCode::TYPE_RESET_PASSWORD,
            'is_used' => 0,
        ]))->count(1);
    }
}