<?php
/**
 *
 * User: psi
 * Date: 23.01.17
 * Time: 23:14
 */

namespace app\tests\unit\models;


use app\models\EventNotification;
use app\models\EventNotificationRecipient;
use app\tests\fixtures\EventNotificationsFixture;
use app\tests\fixtures\UserFixture;
use Codeception\Test\Unit;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;

class EventNotificationTest extends Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
        parent::_before();
        $this->tester->haveFixtures([
            'notifications' => EventNotificationsFixture::class,
            'users' => UserFixture::class,
        ]);
    }

    public function testAddRecipient()
    {
        /** @var EventNotification $n */
        $n = $this->tester->grabFixture('notifications', '*-group');
        $n->addRecipient(100);
        $n->addRecipient(101);
        $n->addRecipient(102);

        $recp = EventNotificationRecipient::findAll(['notification_id' => $n->id]);
        $recp = ArrayHelper::getColumn($recp, 'user_id', false);
        sort($recp);
        expect($recp)->equals([100, 101, 102]);
    }

    /**
     * @expectedException \yii\base\InvalidParamException
     */
    public function testAddRecipientShouldExceptionIfWrongType()
    {
        /** @var EventNotification $n */
        $n = new EventNotification(['recipient_type' => EventNotification::RECIPIENTS_ALL]);
        $n->addRecipient(100);
    }

    public function testValidation()
    {
        $recp = new EventNotificationRecipient(['notification_id' => 103, 'user_id' => 0]);
        $valid = $recp->validate();
        expect_not($valid);
        expect($recp->errors)->count(1);
        expect($recp->errors)->hasKey('user_id');

        $recp = new EventNotificationRecipient(['notification_id' => 0, 'user_id' => 100]);
        $valid = $recp->validate();
        expect_not($valid);
        expect($recp->errors)->count(1);
        expect($recp->errors)->hasKey('notification_id');
    }

    public function testValidationRecipient()
    {
        $model = new EventNotification(['recipient_type' => EventNotification::RECIPIENTS_ALL]);
        $valid = $model->validate(['recipient_type', 'recipient_value']);
        expect_that($valid);

        $model = new EventNotification(['recipient_type' => EventNotification::RECIPIENTS_OWNER]);
        $valid = $model->validate(['recipient_type', 'recipient_value']);
        expect_that($valid);

        $model = new EventNotification(['recipient_type' => EventNotification::RECIPIENTS_GROUP]);
        $valid = $model->validate(['recipient_type', 'recipient_value']);
        expect_that($valid);

        $model = new EventNotification(['recipient_type' => EventNotification::RECIPIENTS_ROLE]);
        $valid = $model->validate(['recipient_type', 'recipient_value']);
        expect_not($valid);
        expect($model->errors)->count(1);
        expect($model->errors)->hasKey('recipient_value');
    }

    public function testAutoremoveShouldNotDeleteModelIfNotAutoremoveFlag()
    {
        $model = $this->getMockBuilder(EventNotification::class)->setConstructorArgs([[
            'is_autoremove' => 0,
        ]])->setMethods(['delete'])->getMock();
        $model->expects($this->never())->method('delete')->with();
        /** @var EventNotification $model */
        $removed = $model->autoremove();
        expect($removed)->false();
    }

    public function testAutoremoveShouldDeleteModel()
    {
        $model = $this->getMockBuilder(EventNotification::class)->setConstructorArgs([[
            'is_autoremove' => 1,
        ]])->setMethods(['delete'])->getMock();
        $model->expects($this->once())->method('delete')->with()->willReturn(1);
        /** @var EventNotification $model */
        $removed = $model->autoremove();
        expect($removed)->true();
    }

    public function testCreateManualNotification()
    {
        /** @var EventNotification $n */
        $n = $this->tester->grabFixture('notifications', '*-group');

        $actual = $n->createManualNotification();
        expect($actual)->notSame($n);
        expect($actual->isNewRecord)->true();
        expect($actual->id)->null();
        foreach (['transport_name', 'target_class', 'target_event', 'content_template'] as $attr) {
            expect("$attr should be equals", $actual->$attr)->equals($n->$attr);
        }
        expect($actual->is_autoremove)->equals(1);
        expect($actual->recipient_type)->null();
        expect($actual->recipient_value)->null();
    }
}