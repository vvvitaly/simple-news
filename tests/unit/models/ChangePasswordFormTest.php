<?php
/**
 *
 * User: psi
 * Date: 19.01.17
 * Time: 23:49
 */

namespace app\tests\unit\models;


use app\models\ChangePasswordForm;
use app\models\User;
use app\models\UserActivationCode;
use app\tests\fixtures\UserFixture;
use Codeception\Test\Unit;

class ChangePasswordFormTest extends Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
        parent::_before();
        $this->tester->haveFixtures(['users' => UserFixture::class]);
    }

    public function testChangePassword()
    {
        $user = User::findOne(100);

        $uac = UserActivationCode::generateForUser($user, UserActivationCode::TYPE_RESET_PASSWORD);
        expect_that($uac->id);
        $uac->refresh();

        $form = new ChangePasswordForm([
            'code' => $uac->code,
            'password' => 'qwerty',
            'password_repeat' => 'qwerty',
        ]);

        expect_that($form->changePassword());
        $user->refresh();
        $uac->refresh();

        expect_that($user->validatePassword('qwerty'));
        expect_that($uac->is_used);
    }

    public function testChangePasswordByUserId()
    {
        $user = User::findOne(100);

        $uac = UserActivationCode::generateForUser($user, UserActivationCode::TYPE_RESET_PASSWORD);
        expect_that($uac->id);
        $uac->refresh();

        $form = new ChangePasswordForm([
            'code' => $uac->code,
            'password' => 'qwerty',
            'password_repeat' => 'qwerty',
        ]);

        expect_that($form->changePassword());
        $user->refresh();
        $uac->refresh();

        expect_that($user->validatePassword('qwerty'));
        expect_that($uac->is_used);
    }

    public function testValidateShouldErrorIfWrongRepeatPassword()
    {
        $user = User::findOne(100);

        $uac = UserActivationCode::generateForUser($user, UserActivationCode::TYPE_RESET_PASSWORD);
        expect_that($uac->id);

        $form = new ChangePasswordForm([
            'code' => $uac->code,
            'password' => 'qwerty',
            'password_repeat' => '123',
        ]);

        expect_not($form->validate());
        expect($form->errors)->count(1);
        expect($form->errors)->hasKey('password');
    }

    public function testValidateShouldErrorIftPasswordIsTooShort()
    {
        $user = User::findOne(100);

        $uac = UserActivationCode::generateForUser($user, UserActivationCode::TYPE_RESET_PASSWORD);
        expect_that($uac->id);

        $form = new ChangePasswordForm([
            'code' => $uac->code,
            'password' => '123',
            'password_repeat' => '123',
        ]);

        expect_not($form->validate());
        expect($form->errors)->count(1);
        expect($form->errors)->hasKey('password');
    }

    public function testValidateShouldRaiseExceptionIfWrongType()
    {
        $user = User::findOne(100);

        $uac = UserActivationCode::generateForUser($user, UserActivationCode::TYPE_ACTIVATE_ACCOUNT);
        expect_that($uac->id);
        $uac->refresh();

        $form = new ChangePasswordForm([
            'activationModel' => $uac,
            'password' => 'qwerty',
            'password_repeat' => 'qwerty',
        ]);


        expect_not($form->validate());
        expect($form->errors)->count(1);
        expect($form->errors)->hasKey('code');
    }

    public function testValidateShouldErrorIfActivationCodeUsed()
    {
        $user = User::findOne(100);

        $uac = UserActivationCode::generateForUser($user, UserActivationCode::TYPE_ACTIVATE_ACCOUNT, null, false);
        $uac->is_used = true;
        $saved = $uac->save();
        expect_that($saved);
        $uac->refresh();

        $form = new ChangePasswordForm([
            'code' => $uac->code,
            'password' => 'qwerty',
            'password_repeat' => 'qwerty',
        ]);

        expect_not($form->validate());
        expect($form->errors)->count(1);
        expect($form->errors)->hasKey('code');
    }

    public function testValidateShouldErrorIfActivationCodeExpires()
    {
        $user = User::findOne(100);

        $uac = UserActivationCode::generateForUser($user, UserActivationCode::TYPE_ACTIVATE_ACCOUNT, null, false);
        $uac->active_to = '2017-01-01 00:00:00';
        $saved = $uac->save();
        expect_that($saved);
        $uac->refresh();

        $form = new ChangePasswordForm([
            'code' => $uac->code,
            'password' => 'qwerty',
            'password_repeat' => 'qwerty',
        ]);

        expect_not($form->validate());
        expect($form->errors)->count(1);
        expect($form->errors)->hasKey('code');
    }

    public function testValidateShouldErrorIfActivationCodeWrong()
    {
        $form = new ChangePasswordForm([
            'code' => '000',
            'password' => 'qwerty',
            'password_repeat' => 'qwerty',
        ]);

        expect_not($form->validate());
        expect($form->errors)->count(1);
        expect($form->errors)->hasKey('code');
    }
}