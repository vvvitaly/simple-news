<?php
/**
 *
 * User: psi
 * Date: 24.01.17
 * Time: 18:19
 */

namespace app\tests\unit\jobs;


use app\events\Notifier;
use app\jobs\NotificationsController;
use Codeception\Test\Unit;

class NotificationsControllerTest extends Unit
{
    public function testActionSend()
    {
        $nid = 100;
        $event = 'some-event';
        $sender = 'some-sender';

        $notifier = $this->getMockBuilder(Notifier::class)
            ->setMethods(['send'])
            ->getMock();

        $notifier
            ->expects($this->once())->method('send')->with($nid, $event, $sender);
        \Yii::$app->set('notifier', $notifier);

        $module = \Yii::$app->getModule('jobs');
        $module->runAction('notifications/send', ['id' => $nid, 'event' => $event, 'sender' => $sender]);
    }
}