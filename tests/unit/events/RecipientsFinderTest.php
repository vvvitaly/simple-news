<?php
/**
 *
 * User: psi
 * Date: 23.01.17
 * Time: 13:11
 */

namespace app\tests\unit\events;


use app\events\RecipientsFinder;
use app\models\EventNotification;
use app\events\NotifiableInterface;
use app\tests\fixtures\AuthAssignmentFixture;
use app\tests\fixtures\EventNotificationsFixture;
use app\tests\fixtures\UserFixture;
use Codeception\Test\Unit;
use yii\helpers\ArrayHelper;

class RecipientsFinderTest extends Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
        parent::_before();
        $this->tester->haveFixtures([
            'users' => UserFixture::class,
            'notifications' => EventNotificationsFixture::class,
            AuthAssignmentFixture::class,
        ]);
    }

    public function testFindAll()
    {
        $n = new EventNotification(['recipient_type' => EventNotification::RECIPIENTS_ALL]);
        $finder = new RecipientsFinder();
        $users = $finder->find($n, null);
        $actual = ArrayHelper::getColumn($users, 'id', false);
        sort($actual);
        expect($actual)->equals([100, 101, 102, 103, 104]);
    }

    public function testFindRole()
    {
        $n = new EventNotification([
            'recipient_type' => EventNotification::RECIPIENTS_ROLE,
            'recipient_value' => 'user',
        ]);
        $finder = new RecipientsFinder();
        $users = $finder->find($n, null);
        $actual = ArrayHelper::getColumn($users, 'id', false);
        sort($actual);
        expect($actual)->equals([102, 103, 104]);
    }

    public function testFindGroup()
    {
        /** @var EventNotification $n */
        $n = $this->tester->grabFixture('notifications', '*-group');
        $n->addRecipient(100);
        $n->addRecipient(102);

        $finder = new RecipientsFinder();
        $users = $finder->find($n, null);
        $actual = ArrayHelper::getColumn($users, 'id', false);
        sort($actual);
        expect($actual)->equals([100, 102]);
    }

    public function testFindOwner()
    {
        $model = $this->getMockBuilder(NotifiableInterface::class)->setMethods(['getOwnerId'])->getMock();
        $model->expects($this->once())->method('getOwnerId')->with()->willReturn(100);

        $n = new EventNotification([
            'recipient_type' => EventNotification::RECIPIENTS_OWNER,
        ]);

        $finder = new RecipientsFinder();
        $users = $finder->find($n, $model);
        $actual = ArrayHelper::getColumn($users, 'id', false);
        expect($actual)->equals([100]);
    }

    /**
     * @expectedException \yii\base\InvalidParamException
     */
    public function testFindOwnerWrongSender()
    {
        $n = new EventNotification([
            'recipient_type' => EventNotification::RECIPIENTS_OWNER,
        ]);

        $finder = new RecipientsFinder();
        $finder->find($n, new \stdClass());
    }
}