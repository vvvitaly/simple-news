<?php
/**
 *
 * User: psi
 * Date: 22.01.17
 * Time: 21:45
 */

namespace app\tests\unit\events;


use app\events\MessageInterface;
use app\events\TransportInterface;
use app\events\EventSerializer;
use app\events\Notifier;
use app\events\RecipientsFinder;
use app\events\Renderer;
use app\events\RouterInterface;
use app\models\EventNotification;
use app\models\User;
use Codeception\Test\Unit;
use UrbanIndo\Yii2\Queue\Job;
use UrbanIndo\Yii2\Queue\Queue;
use yii\base\Component;
use yii\base\Event;


class _NotifierTest_TargetClass1 extends Component
{
    const EVENT1 = 'event1';
    const EVENT2 = 'event2';
}

class _NotifierTest_TargetClass2Event extends Event
{
    public $params;
}

class _NotifierTest_TargetClass2 extends Component
{
    const EVENT1 = 'event1';
}

class NotifierTest extends Unit
{
    public function testListen()
    {
        $target1 = new _NotifierTest_TargetClass1();
        $uniq =  uniqid();
        $target2 = new _NotifierTest_TargetClass2();

        $e1 = new Event(['sender' => $target1, 'name' => _NotifierTest_TargetClass1::EVENT1]);
        $e2 = new Event(['sender' => $target1, 'name' => _NotifierTest_TargetClass1::EVENT2]);
        $e3 = new _NotifierTest_TargetClass2Event([
            'sender' => $target2,
            'name' => _NotifierTest_TargetClass1::EVENT1,
            'params' => $uniq
        ]);

        $n = $this->getMockBuilder(Notifier::class)
            ->setMethods(['routeEvent'])
            ->setConstructorArgs([[
                'events' => [
                    [_NotifierTest_TargetClass1::class, _NotifierTest_TargetClass1::EVENT1],
                    [_NotifierTest_TargetClass2::class, _NotifierTest_TargetClass2::EVENT1],
                ],
            ]])
            ->getMock();

        // $target1->trigger(_NotifierTest_TargetClass1::EVENT1, $e1); TWO TIMES!!
        $n->expects($this->at(0))->method('routeEvent')->with($e1);
        $n->expects($this->at(1))->method('routeEvent')->with($e1);

        // $target1->trigger(_NotifierTest_TargetClass1::EVENT2, $e2); NOT LISTENING!!

        // $target2->trigger(_NotifierTest_TargetClass2::EVENT1, $e3);
        $n->expects($this->at(2))->method('routeEvent')->with($e3);

        // total calls
        $n->expects($this->exactly(3))->method('routeEvent');

        /** @var Notifier $n */
        $n->listen();

        $target1->trigger(_NotifierTest_TargetClass1::EVENT1, $e1);
        $target1->trigger(_NotifierTest_TargetClass1::EVENT2, $e2);
        $target1->trigger(_NotifierTest_TargetClass1::EVENT1, $e1);
        $target2->trigger(_NotifierTest_TargetClass2::EVENT1, $e3);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testListenObject()
    {
        $n = new Notifier([
            'events' => [
                [new _NotifierTest_TargetClass1(), 'some'],
            ],
        ]);
        $n->listen();
    }

    public function testRouteEvent()
    {
        $n1 = new EventNotification([
            'id' => 1,
            'transport_name' => 'dr1',
            'target_class' => _NotifierTest_TargetClass1::class,
            'target_event' => 'event1',
            'recipient_type' => 'all',
            'content_template' => '',
        ]);
        $n2 = new EventNotification([
            'id' => 2,
            'transport_name' => 'dr2',
            'target_class' => '*',
            'target_event' => '*',
            'recipient_type' => 'all',
            'content_template' => '',
        ]);

        $event = new Event([
            'sender' => new _NotifierTest_TargetClass1(),
            'name' => _NotifierTest_TargetClass1::EVENT1
        ]);

        $mockRouter = $this->getMockBuilder(RouterInterface::class)
            ->setMethods(['findNotifications'])
            ->getMock();

        $mockRouter->expects($this->once())->method('findNotifications')->with($event)->willReturn([$n1, $n2]);

        $notifier  = $this->getMockBuilder(Notifier::class)
            ->setConstructorArgs([['router' => $mockRouter]])
            ->setMethods(['applyNotification'])
            ->getMock();
        $notifier->expects($this->at(0))->method('applyNotification')->with($n1, $event);
        $notifier->expects($this->at(1))->method('applyNotification')->with($n2, $event);
        $notifier->expects($this->exactly(2))->method('applyNotification');

        /** @var Notifier $notifier */
        $notifier->routeEvent($event);
    }

    public function testApplyNotification()
    {
        $notification = new EventNotification([
            'id' => 1,
            'transport_name' => 'dr1',
            'target_class' => _NotifierTest_TargetClass1::class,
            'target_event' => 'event1',
            'recipient_type' => 'all',
            'content_template' => '',
        ]);

        $e = new Event([
            'sender' => new _NotifierTest_TargetClass1(),
            'name' => _NotifierTest_TargetClass1::EVENT1
        ]);

        $mockSerializer = $this->getMockBuilder(EventSerializer::class)->setMethods(['serializeSender'])->getMock();
        $mockSerializer->expects($this->once())
            ->method('serializeSender')
            ->with($e->sender)
            ->willReturn('serialized-sender');

        $mockQueue = $this->getMockBuilder(Queue::class)->setMethods(['post'])->getMockForAbstractClass();
        $mockQueue->expects($this->once())->method('post')->with($this->callback(function ($job) use($notification, $e) {
            return $job instanceof Job && $job->route === 'notifications/send' &&
                $job->data === [
                    'id' => $notification->id,
                    'event' => _NotifierTest_TargetClass1::EVENT1,
                    'sender' => 'serialized-sender'
                ];
        }));
        $n = new Notifier([
            'queue' => $mockQueue,
            'serializer' => $mockSerializer,
        ]);

        $n->applyNotification($notification, $e);
    }

    public function testSend()
    {
        $notification = $this->getMockBuilder(EventNotification::class)->setConstructorArgs([[
            'id' => 1,
            'transport_name' => 'dr1',
            'target_class' => _NotifierTest_TargetClass1::class,
            'target_event' => 'event1',
            'recipient_type' => 'all',
            'content_template' => 'some template',
            'is_autoremove' => 0,
        ]])->setMethods(['autoremove'])->getMock();
        $notification->expects($this->once())->method('autoremove')->with();
        /** @var EventNotification $notification */

        $user1 = new User([
            'id' => 1,
            'email' => 'user1@example.com',
            'name' => 'User1',
        ]);
        $user2 = new User([
            'id' => 2,
            'email' => 'user2@example.com',
            'name' => 'User2',
        ]);

        $target = new _NotifierTest_TargetClass1();

        // unserialize event
        $mockSerializer = $this->getMockBuilder(EventSerializer::class)->setMethods(['unserializeSender'])->getMock();
        $mockSerializer->expects($this->once())
            ->method('unserializeSender')
            ->with('serialized-sender')
            ->willReturn($target);

        // render contents for the event
        $render = $this->createPartialMock(Renderer::class, ['render']);

        $messageRendered = $this->createMock(MessageInterface::class);
        $message = $this->createMock(MessageInterface::class);
        $message->expects($this->once())->method('render')->with($render, $target)->willReturn($messageRendered);

        // one transport, but two recipients & can not send to user2
        $transport = $this->createMock(TransportInterface::class);
        $transport->expects($this->once())->method('unserializeMessageModel')->with('some template')->willReturn($message);

        $transport->expects($this->at(1))->method('canSend')->with($user1)->willReturn(true);
        $transport->expects($this->at(2))->method('send')->with($user1, $messageRendered);
        $transport->expects($this->at(3))->method('canSend')->with($user2)->willReturn(false);
        $transport->expects($this->exactly(2))->method('canSend');
        $transport->expects($this->exactly(1))->method('send');

        // parse recipient type & get recipients (e.g. all - all users)
        $finder = $this->createPartialMock(RecipientsFinder::class, ['find']);
        $finder->expects($this->once())->method('find')->with($notification, $target)->willReturn([$user1, $user2]);

        $notifier = $this->getMockBuilder(Notifier::class)
            ->setConstructorArgs([[
                'serializer' => $mockSerializer,
                'transports' => ['dr1' => $transport],
                'renderer' => $render,
                'recipientsFinder' => $finder
            ]])
            ->setMethods(['findNotification', 'getTransport'])
            ->getMock();

        $notifier
            ->expects($this->once())->method('getTransport')->with($notification->transport_name)->willReturn($transport);
        $notifier
            ->expects($this->once())->method('findNotification')->with($notification->id)->willReturn($notification);

        /** @var Notifier $notifier */
        $notifier->send($notification->id, 'event1', 'serialized-sender');
    }

    public function testSendWithEmptyEvent()
    {
        $notification = $this->getMockBuilder(EventNotification::class)->setConstructorArgs([[
            'id' => 1,
            'transport_name' => 'dr1',
            'target_class' => _NotifierTest_TargetClass1::class,
            'target_event' => 'event1',
            'recipient_type' => 'all',
            'content_template' => 'some template',
            'is_autoremove' => 0,
        ]])->setMethods(['autoremove'])->getMock();
        $notification->expects($this->once())->method('autoremove')->with();
        /** @var EventNotification $notification */

        $user1 = new User([
            'id' => 1,
            'email' => 'user1@example.com',
            'name' => 'User1',
        ]);
        $user2 = new User([
            'id' => 2,
            'email' => 'user2@example.com',
            'name' => 'User2',
        ]);

        $target = new _NotifierTest_TargetClass1();

        // unserialize event
        $mockSerializer = $this->getMockBuilder(EventSerializer::class)->setMethods(['unserializeSender'])->getMock();
        $mockSerializer->expects($this->once())
            ->method('unserializeSender')
            ->with('serialized-sender')
            ->willReturn($target);

        // render contents for the event
        $render = $this->createPartialMock(Renderer::class, ['render']);

        $messageRendered = $this->createMock(MessageInterface::class);
        $message = $this->createMock(MessageInterface::class);
        $message->expects($this->once())->method('render')->with($render, $target)->willReturn($messageRendered);

        // one transport, but two recipients & can not send to user2
        $transport = $this->createMock(TransportInterface::class);
        $transport->expects($this->once())->method('unserializeMessageModel')->with('some template')->willReturn($message);

        $transport->expects($this->at(1))->method('canSend')->with($user1)->willReturn(true);
        $transport->expects($this->at(2))->method('send')->with($user1, $messageRendered);
        $transport->expects($this->at(3))->method('canSend')->with($user2)->willReturn(false);
        $transport->expects($this->exactly(2))->method('canSend');
        $transport->expects($this->exactly(1))->method('send');

        // parse recipient type & get recipients (e.g. all - all users)
        $finder = $this->createPartialMock(RecipientsFinder::class, ['find']);
        $finder->expects($this->once())->method('find')->with($notification, $target)->willReturn([$user1, $user2]);

        $notifier = $this->getMockBuilder(Notifier::class)
            ->setConstructorArgs([[
                'serializer' => $mockSerializer,
                'transports' => ['dr1' => $transport],
                'renderer' => $render,
                'recipientsFinder' => $finder
            ]])
            ->setMethods(['findNotification'])
            ->getMock();

        $notifier
            ->expects($this->once())->method('findNotification')->with($notification->id)->willReturn($notification);

        /** @var Notifier $notifier */
        $notifier->send($notification->id, 'event1', 'serialized-sender');
    }
}