<?php
/**
 *
 * User: psi
 * Date: 25.01.17
 * Time: 1:09
 */

namespace app\tests\unit\events\transports;


use app\events\Renderer;
use app\events\transports\EmailMessageModel;
use Codeception\Test\Unit;

class EmailMessageModelTest extends Unit
{
    public function testRender()
    {
        $sender = new \stdClass();
        $render = $this->createMock(Renderer::class);
        $render->method('render')->will($this->returnValueMap([
            ['subj-template', $sender, 'subj'],
            ['body-template', $sender, 'body'],
        ]));

        $model = new EmailMessageModel(['subject' => 'subj-template', 'body' => 'body-template']);
        $actual = $model->render($render, $sender);
        expect($actual)->isInstanceOf(EmailMessageModel::class);
        expect($actual->subject)->equals('subj');
        expect($actual->body)->equals('body');
    }
}