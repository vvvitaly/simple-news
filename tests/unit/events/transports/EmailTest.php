<?php
/**
 *
 * User: psi
 * Date: 22.01.17
 * Time: 21:51
 */

namespace app\tests\unit\events\transports;


use app\events\transports\Email;
use app\events\transports\EmailMessageModel;
use app\models\User;
use Codeception\Test\Unit;
use yii\mail\MailerInterface;
use yii\mail\MessageInterface;

class EmailTest extends Unit
{
    public function testCanSendForUserSubscribed()
    {
        $u = new User(['is_allow_emails' => 1]);

        $tr = new Email();
        expect($tr->canSend($u))->true();
    }

    public function testCanNotSendForUserUnSubsdcribed()
    {
        $u = new User(['is_allow_emails' => 0]);

        $tr = new Email();
        expect($tr->canSend($u))->false();
    }

    public function testSend()
    {
        $u = new User([
            'is_allow_emails' => 1,
            'email' => 'test@example.com',
            'name' => 'test',
        ]);

        $message = $this->createMock(MessageInterface::class);
        $message->expects($this->once())->method('setTo')->with(['test@example.com' => 'test'])->willReturnSelf();
        $message->expects($this->once())->method('setFrom')->withAnyParameters()->willReturnSelf();
        $message->expects($this->once())->method('setSubject')->with('subj')->willReturnSelf();
        $message->expects($this->once())->method('setHtmlBody')->with('body')->willReturnSelf();
        $message->expects($this->once())->method('send');

        $mailer = $this->createMock(MailerInterface::class);
        $mailer->expects($this->once())->method('compose')->willReturn($message);

        $tr = new Email([
            'mailer' => $mailer,
        ]);
        $tr->send($u, new EmailMessageModel(['subject' => 'subj', 'body' => 'body']));
    }
}