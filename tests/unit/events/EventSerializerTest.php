<?php
/**
 *
 * User: psi
 * Date: 23.01.17
 * Time: 21:20
 */

namespace app\tests\unit\events;


use app\events\EventSerializer;
use app\models\User;
use app\tests\fixtures\UserFixture;
use Codeception\Test\Unit;

class EventSerializerTest extends Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
        parent::_before();
        $this->tester->haveFixtures([
            'users' => UserFixture::class,
        ]);
    }

    public function testSerializeSenderActiveRecord()
    {
        $u = $this->tester->grabFixture('users', 0);

        $s = new EventSerializer();
        $actual = $s->serializeSender($u);
        expect($actual)->equals([':ar:', User::class, 100]);
    }

    public function testSerializeSenderOther()
    {
        $obj = new \stdClass();

        $s = new EventSerializer();
        $actual = $s->serializeSender($obj);
        expect($actual)->same($obj);
    }

    public function testUnserializeSenderActiveRecord()
    {
        $s = new EventSerializer();
        $actual = $s->unserializeSender([':ar:', User::class, 100]);
        expect($actual)->equals($this->tester->grabFixture('users', 0));
    }

    public function testSerializeSenderNull()
    {
        $s = new EventSerializer();
        $actual = $s->serializeSender(null);
        expect($actual)->equals([':null:']);
    }

    public function testUnserializeSenderNull()
    {
        $s = new EventSerializer();
        $actual = $s->unserializeSender([':null:']);
        expect($actual)->null();
    }
}