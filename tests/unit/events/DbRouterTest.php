<?php
/**
 *
 * User: psi
 * Date: 22.01.17
 * Time: 22:40
 */

namespace app\tests\unit\events;


use app\events\DbRouter;
use app\tests\fixtures\EventNotificationsFixture;
use Codeception\Test\Unit;
use yii\base\Component;
use yii\base\Event;
use yii\helpers\ArrayHelper;

class _DbRouterTest_TargetClass1 extends Component
{
}

class _DbRouterTest_TargetClass2 extends Component
{
}

class DbRouterTest extends Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
        parent::_before();
        $this->tester->haveFixtures([
            'events_routes' => EventNotificationsFixture::class,
        ]);
    }

    public function _event($sender, $name)
    {
        return new Event(['sender' => $sender, 'name' => $name]);
    }

    public function testFindNotifications()
    {
        $router = new DbRouter();

        $actual = $router->findNotifications($this->_event(new _DbRouterTest_TargetClass1(), 'event1'));
        $actual = ArrayHelper::getColumn($actual, 'id', false);
        sort($actual);
        expect($actual)->equals([100, 101, 102, 103]);

        $actual = $router->findNotifications($this->_event(new _DbRouterTest_TargetClass1(), 'event100500'));
        $actual = ArrayHelper::getColumn($actual, 'id', false);
        sort($actual);
        expect($actual)->equals([103]);

        $actual = $router->findNotifications($this->_event(new _DbRouterTest_TargetClass2(), 'event1'));
        $actual = ArrayHelper::getColumn($actual, 'id', false);
        sort($actual);
        expect($actual)->equals([101, 102, 103]);
    }
}