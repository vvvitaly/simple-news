<?php
/**
 *
 * User: psi
 * Date: 23.01.17
 * Time: 9:46
 */

namespace app\tests\unit\events;


use app\events\Renderer;
use app\models\UserActivationCode;
use app\tests\fixtures\UserFixture;
use Codeception\Test\Unit;

class RendererTest extends Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
        parent::_before();
        $this->tester->haveFixtures([
            'users' => UserFixture::class,
        ]);
    }

    public function testRender()
    {
        $user = $this->tester->grabFixture('users', 0);
        $uac = UserActivationCode::generateForUser($user, UserActivationCode::TYPE_ACTIVATE_ACCOUNT);
        $uac->save();

        $tpl = <<<EOT
User #{{sender.id}} ({{sender.email}})
Code: {{sender.currentAccountActivationCode.code}}
EOT;

        $expected = <<<EOT
User #{$user->id} ({$user->email})
Code: {$uac->code}
EOT;

        $r = new Renderer();
        $actual = $r->render($tpl, $user);
        expect($actual)->equals($expected);
    }
}