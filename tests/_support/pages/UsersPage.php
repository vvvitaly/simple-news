<?php
/**
 *
 * User: psi
 * Date: 22.01.17
 * Time: 9:45
 */

namespace pages;


class UsersPage
{
    const LIST_URL = '/users/index';
    const EDIT_URL = '/users/update';
    const CREATE_URL = '/users/create';
    const REMOVE_URL = '/users/delete';
    const RESET_PASSWORD_URL = '/users/reset-password';

    const GRID_ELEMENT = '.grid-view table';
    const GRID_CELL_TPL = '//div[contains(@class, "grid-view")]/table/tbody/tr[@data-key="{userId}"]/td[{cellIdx}]';
    const ERROR_ELEMENT_TPL = 'div.field-user-{attribute} div.help-block';

    const EDIT_FORM = '.user-form form';
    const RESET_PASSWORD_BTN = '#reset-password-form button[type=submit]';

    /**
     * Get grid cell element by user id & ttribute
     * @param $userId
     * @param $attribute
     * @return string
     */
    public static function gridCellByUserId($userId, $attribute)
    {
        $cellsIndexes = [
            'created_at' => 1,
            'email' => 2,
            'name' => 3,
            'is_activated' => 4,
        ];

        return strtr(self::GRID_CELL_TPL, ['{userId}' => $userId, '{cellIdx}' => $cellsIndexes[$attribute]]);
    }

    /**
     * Get form error block by attribute
     * @param $attribute
     * @return string
     */
    public static function errorElement($attribute)
    {
        return strtr(self::ERROR_ELEMENT_TPL, ['{attribute}' => $attribute]);
    }
}