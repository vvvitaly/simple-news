<?php
/**
 *
 * User: psi
 * Date: 22.01.17
 * Time: 17:09
 */

namespace pages;


class NewsPage
{
    const LIST_URL = '/news/index';
    const VIEW_URL = '/news/view';

    const MENU_ELEMENT = '#top-menu';
    const ARTICLE_VIEW_TITLE_ELEMENT = '.news-view h1';
}