<?php
use app\models\User;
use app\models\UserActivationCode;


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
class FunctionalTester extends \Codeception\Actor
{
    use _generated\FunctionalTesterActions;


    public function signup($email, $password, $name)
    {
        $I = $this;
        $I->amOnRoute('site/signup');
        $I->submitForm('#signup-form', [
            'SignupForm[email]' => $email,
            'SignupForm[password]' => $password,
            'SignupForm[password_repeat]' => $password,
            'SignupForm[name]' => $name,
        ]);
        $I->seeRecord(User::class, ['email' => $email]);
        $I->see('Logout (');
        $I->dontSeeElement('form#signup-form');
    }

    public function login($email, $password)
    {
        $I = $this;
        $I->amOnRoute('site/login');
        $I->submitForm('form#login-form', [
            'LoginForm[email]' => $email,
            'LoginForm[password]' => $password,
        ]);
        $I->see('Logout (');
    }

    public function resetPasswordAs($user)
    {
        $I = $this;
        $I->amLoggedInAs($user);

        $I->amOnRoute('/profile');
        $I->submitForm('form#reset-password-form', []);
        $I->see('You sent an email with a password reset link.');
    }

    /**
     * @param $user
     * @return UserActivationCode
     */
    public function grabResetPasswordCodeFor($user)
    {
        $I = $this;
        return $I->grabRecord(UserActivationCode::class, [
            'user_id' => $user,
            'type' => UserActivationCode::TYPE_RESET_PASSWORD,
            'is_used' => 0
        ]);
    }
}
