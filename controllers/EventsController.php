<?php

namespace app\controllers;

use app\auth\Permissions;
use app\events\Notifier;
use app\events\TransportInterface;
use app\models\User;
use Yii;
use app\models\EventNotification;
use yii\base\Event;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\Inflector;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EventsController implements the CRUD actions for EventNotification model.
 */
class EventsController extends Controller
{
    /**
     * @var string[]
     */
    private $_transports;

    /**
     * @var array
     */
    private $_events;

    /**
     * @var string[]
     */
    private $_initiators;

    /**
     * @var array
     */
    private $_roles;

    private function getNotifierEvents()
    {
        $events = Yii::$app->notifier->events;
        $this->_events = [];
        foreach ($events as list($target, $event)) {
            if (!isset($this->_events[$target])) {
                $this->_events[$target] = [];
            }
            $this->_events[$target][$event] = Inflector::titleize($event);
        }
    }

    public function getTransports()
    {
        if ($this->_transports === null) {
            $this->_transports = [];
            foreach (Yii::$app->notifier->transports as $name => $_) {
                $this->_transports[$name] = Inflector::titleize($name);
            }
        }
        return $this->_transports;
    }

    public function getInitiators()
    {
        if ($this->_initiators === null) {
            $this->getNotifierEvents();
            $keys = array_keys($this->_events);
            $this->_initiators = array_combine($keys, $keys);
        }
        return $this->_initiators;
    }

    public function getEvents()
    {
        if ($this->_events === null) {
            $this->getNotifierEvents();
        }
        return $this->_events;
    }

    public function getRoles()
    {
        if ($this->_roles === null) {
            $roles = Yii::$app->authManager->getRoles();
            $this->_roles = [];
            foreach ($roles as $role) {
                if (!in_array($role->name, [Permissions::ROLE_GUEST])) {
                    $this->_roles[] = $role;
                }
            }
        }

        return $this->_roles;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['view', 'index'],
                        'roles' => [Permissions::VIEW_NOTIFICATIONS],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'roles' => [Permissions::CREATE_NOTIFICATION],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update'],
                        'roles' => [Permissions::EDIT_NOTIFICATION],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['edit-message'],
                        'roles' => [Permissions::EDIT_NOTIFICATION],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['manual', 'add-users', 'add-user', 'send'],
                        'roles' => [Permissions::SEND_NOTIFICATION],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'roles' => [Permissions::REMOVE_NOTIFICATION],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'add-user' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all EventNotification models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => EventNotification::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EventNotification model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new EventNotification model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new EventNotification();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'transports' => $this->getTransports(),
            'initiators' => $this->getInitiators(),
            'events' => $this->_events,
        ]);
    }

    /**
     * Updates an existing EventNotification model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', ['model' => $model]);
    }

    /**
     * Deletes an existing EventNotification model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionEditMessage($id)
    {
        $notification = $this->findModel($id);
        $transport = $notification->transport_name;

        /** @var Notifier $notifier */
        $notifier = Yii::$app->notifier;
        if (!isset($notifier->transports[$transport])) {
            throw new NotFoundHttpException('Wrong transport name');
        }
        /** @var TransportInterface $transport */
        $transport = $notifier->transports[$transport];
        $modelClass = $transport->getMessageModelClass();
        /** @var Model $messageModel */
        $messageModel = $transport->unserializeMessageModel($notification->content_template);
        $view = $transport->getMessageFormView();

        if ($messageModel->load(Yii::$app->request->post()) && $messageModel->validate()) {
            $notification->content_template = $transport->serializeMessageModel($messageModel);
            $notification->save();
            $this->redirect(['view', 'id' => $notification->id]);
        }

        return $this->render($view, ['model' => $messageModel, 'notification' => $notification]);
    }

    public function actionManual($id)
    {
        $notification = $this->findModel($id)->createManualNotification();
        $notification->recipient_type = EventNotification::RECIPIENTS_ALL;

        if ($notification->load(Yii::$app->request->post()) && $notification->save()) {
            $route = $notification->recipient_type === EventNotification::RECIPIENTS_GROUP?
                ['add-users', 'id' => $notification->id]:
                ['send', 'id' => $notification->id];
            return $this->redirect($route);
        }

        return $this->render('manual', [
            'model' => $notification,
            'originalId' => $id,
        ]);
    }

    public function actionAddUsers($id)
    {
        $notification = $this->findModel($id);
        $provider = new ActiveDataProvider([
            'query' => User::find()->with('notifications'),
        ]);

        return $this->render('add_users', [
            'notification' => $notification,
            'dataProvider' => $provider,
        ]);
    }

    public function actionAddUser($id, $user, $remove)
    {
        $notification = $this->findModel($id);
        if ($remove) {
            $notification->removeRecipient($user);
        } else {
            $notification->addRecipient($user);
        }
    }

    public function actionSend($id)
    {
        $notification = $this->findModel($id);
        /** @var Notifier $notifier */
        $notifier = Yii::$app->notifier;

        $notifier->applyNotification($notification, new Event(['name' => $this->uniqueId]));
        Yii::$app->session->addFlash('alert-success', 'Notification was sent!');
        return $this->render('send');
    }

    /**
     * Finds the EventNotification model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EventNotification the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EventNotification::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
