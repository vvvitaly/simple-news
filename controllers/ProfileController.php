<?php
/**
 *
 * User: psi
 * Date: 19.01.17
 * Time: 22:04
 */

namespace app\controllers;


use app\models\ChangePasswordForm;
use app\models\User;
use app\models\UserActivationCode;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class ProfileController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'except' => ['change-password'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'reset-password' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $model = Yii::$app->user->identity;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('alert-success', 'Profile was saved!');
            return $this->redirect(['index']);
        }
        return $this->render('index', ['model' => $model]);
    }

    public function actionResetPassword()
    {
        /** @var User $user */
        $user = Yii::$app->user->identity;
        $user->resetPassword();

        Yii::$app->session->addFlash('alert-success', $this->renderPartial('alert_reset_password'));
        $this->redirect(['index']);
    }

    public function actionChangePassword($code)
    {
        $model = new ChangePasswordForm([
            'code' => $code,
        ]);

        $model->validate(['code']);
        $allowForm = !$model->hasErrors();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->changePassword()) {
                Yii::$app->session->addFlash('alert-success', 'Your password was changed!');
                return $this->redirect(['/profile']);
            }
        }

        return $this->render('change_password', ['model' => $model, 'allowForm' => $allowForm]);
    }
}