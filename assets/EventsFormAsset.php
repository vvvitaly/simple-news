<?php
/**
 *
 * User: psi
 * Date: 24.01.17
 * Time: 15:44
 */

namespace app\assets;


use yii\web\AssetBundle;

class EventsFormAsset extends AssetBundle
{
    public $sourcePath = '@app/static/js';
    public $js = [
        'events-form.js'
    ];
}